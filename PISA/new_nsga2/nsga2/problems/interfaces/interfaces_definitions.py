import math

from nsga2 import seq
from nsga2.problems.problem_definitions import ProblemDefinitions
import numpy as np

class InterfacesDefinitions(ProblemDefinitions):

    def __init__(self,nb_apps,nb_ifaces,array_apps,array_ifaces):
        self.nb_apps = nb_apps
        self.nb_ifaces = nb_ifaces
        self.array_apps = array_apps
        self.array_ifaces = array_ifaces

    def fitness_interfaces2(self, features):
        """Calculates each individuals fitness"""
        E = self.eval_energy(features)
        B = self.eval_bandwidth(features)
        return [B, E, 0]
        
    def eval_energy(self,array_solution):
        nb_apps = self.nb_apps
        nb_ifaces = self.nb_ifaces
        array_apps = self.array_apps
        array_ifaces = self.array_ifaces
        aux, res, avb = 0.,0.,0.
        energy = 0.
        energy_gral = 0.

        ratio_bdw = 0.00
        time_min = 1000000.00
        ind = {}
        ind['TB'] = [None] * nb_ifaces  #SAVE TI TB FOR nb_ifaces INTERFACES
        ind['TI'] = [None] * nb_ifaces  #SAVE TI TB FOR nb_ifaces INTERFACES

        ind['energy_array'] = [None] * nb_ifaces 

        TB = [None] * nb_ifaces
        TI = [None] * nb_ifaces

        array_if = [0.] * nb_ifaces

        # array_if has the bandwidth demand for each interface
        for i in range(nb_apps):
            bdw_total = float(array_apps[i]['bdw'])
            array_if[array_solution[i]] += bdw_total
            if float(array_apps[i]['time']) < time_min:
                time_min = float(array_apps[i]['time'])


        #we calculate the Tb and Ti for each interface
        for i in range(nb_ifaces):
            ratio_bdw = float(array_if[i]) / float(array_ifaces[i]['av_bdw'])

            if ratio_bdw <= 1:
                TB[i] = ratio_bdw * time_min
                TI[i] = time_min - TB[i]
            else:
                TB[i] = time_min
                TI[i] = 0.

        
        ##for each interface, we calculate the energy consumed
        energy = 0.

        for i in range(nb_ifaces):
            if TB[i] > 0: #if the interface is used
                if array_ifaces[i]['type'] == 0: #3G
                    if TI[i] < array_ifaces[i]['timer1']:
                        energy = float(TB[i]+TI[i]) * float(array_ifaces[i]['pow1'])
                    elif TI[i] < (array_ifaces[i]['timer1'] + array_ifaces[i]['timer2']):
                        energy = (TB[i] + float(array_ifaces[i]['timer1'])) * float(array_ifaces[i]['pow1']) + (TI[i] - float(array_ifaces[i]['timer1'])) * float(array_ifaces[i]['pow2'])
                    else:
                        energy = (TB[i]+float(array_ifaces[i]['timer1'])) * float(array_ifaces[i]['pow1']) + (float(array_ifaces[i]['timer2']) * float(array_ifaces[i]['pow2']) + (TI[i]-float(array_ifaces[i]['timer1']) - float(array_ifaces[i]['timer2'])) * float(array_ifaces[i]['pow3']))
                else: #WLAN
                    if TI[i] < array_ifaces[i]['timer1']:
                        energy =  TB[i]*float(array_ifaces[i]['pow1']) + TI[i] * float(array_ifaces[i]['pow2'])
                    else:
                        energy = TB[i]*float(array_ifaces[i]['pow1']) + float(array_ifaces[i]['timer1']) * float(array_ifaces[i]['pow2']) + (TI[i]-float(array_ifaces[i]['timer1'])) * float(array_ifaces[i]['pow3'])

                ind['energy_array'][i] = energy

                if energy != 0:
                    energy_gral += (float(energy))/(1000.)/(float(TB[i]+TI[i]))

        for i in range(nb_ifaces):
            ind['TB'][i]=TB[i]
            ind['TI'][i]=TI[i]
        return energy_gral
            
    def eval_bandwidth(self,array_solution):
        nb_apps = self.nb_apps
        nb_ifaces = self.nb_ifaces
        array_apps = self.array_apps
        array_ifaces = self.array_ifaces
        avb = 0.;
        aux = 0.;
        res = 0.;
        array_if = [0.] * nb_ifaces


        for i in range(nb_apps):
            bdw_total = float(array_apps[i]['bdw']);
            array_if[array_solution[i]] += bdw_total;


        #SUM OF DEMAND EXCESS
        for i in range(nb_ifaces):
            if array_if[i] > 0:   # Only if the interface is used
                avb = float(array_ifaces[i]['av_bdw']);
                aux = float(array_if[i]);

                if (avb - aux < 0):
                    res += float(avb - aux);

        return abs(res);

