"""Module with definition of Selecting Interfaces problem"""

from nsga2.individual import Individual
from nsga2.individual import IndividualSpark
from nsga2.problems import Problem
import random
import functools
import numpy as np

class Interfaces(Problem):

    def __init__(self, interfaces_definitions,n,m):
        self.interfaces_definitions = interfaces_definitions
        self.max_objectives = [0., 0., 0.]
        self.min_objectives = [9e12, 9e12, 9e12]
        self.problem_type = None
        self.n = n # amount of genes
        self.m = m # max value of gene

    def dominatesS_notEval(self, objectives2, objectives1): #2 objectives positives one negative?
        worse_than_other = objectives1[0] >= objectives2[0] and objectives1[1] <= objectives2[1] and objectives1[2] <= objectives2[2]
        better_than_other = objectives1[0] > objectives2[0] or objectives1[1] < objectives2[1] or objectives1[2] < objectives2[2]
        return worse_than_other and better_than_other

    def generateIndividualSpark(self):
        features = []
        random.seed()
        for i in range(self.n):
            features.append(random.randint(0,self.m))
        j = 0
        
        objectives = self.calculate_objectives_Spark(features)
        hash_value = self.calculate_hash(features)    

        return [features, objectives, 0, [], 0, 0, hash_value]

    def calculate_objectives(self, individual):
        individual.objectives = []
        individual.objectives.append(self.interfaces_definitions.f1(individual))
        individual.objectives.append(self.interfaces_definitions.f2(individual))
        for i in range(2):
            if self.min_objectives[i] is None or individual.objectives[i] < self.min_objectives[i]:
                self.min_objectives[i] = individual.objectives[i]
            if self.max_objectives[i] is None or individual.objectives[i] > self.max_objectives[i]:
                self.max_objectives[i] = individual.objectives[i]

    def calculate_objectives_Spark(self, features):
        return self.interfaces_definitions.fitness_interfaces2(features)

    def define_min_maxS(self,objs):
        #print objs
        for i in range(3):
            if  self.min_objectives[i] > objs[i]:
                self.min_objectives[i] = objs[i]
            if self.max_objectives[i] < objs[i]:
                self.max_objectives[i] = objs[i]

    def define_min_maxS2(self,minS,maxS):
        for i in range(3):
            self.min_objectives[i] = minS[i]
            self.max_objectives[i] = maxS[i]

    def calculate_hash(self, features):        
        features_for_hash = str(features)
        hash_value = hash(features_for_hash)
        return hash_value
