#!/usr/bin/perl

open DIFF_MADM_MO,"diff_madm_mo";
open AVG, ">avg_diff";
open DIFF_TOPSIS_MO,"diff_topsis_mo";
open AVG_TOPSIS, ">avg_diff_topsis";

my %hash_count = ();
my %hash_count_topsis = ();
my %hash_avg_mew = ();
my %hash_avg_mew2 = ();
my %hash_avg = ();
my %hash_avg2 = ();
my %hash_avg_topsis = ();
my %hash_avg_topsis2 = ();

foreach my $line (<DIFF_MADM_MO>)	
{
	my @line_array = split ('\t', $line);

	my $gen = $line_array[0];
	my $chro = $line_array[1];
	my $diff = $line_array[2];
	#my $diff_topsis = $line_array[4];
	my $diff_saw_iter = $line_array[6];
	my $diff_mew_iter = $line_array[8];
	my $diff_topsis_iter = $line_array[10];
	my $diff_gap = $line_array[12];
	my $diff_mew = $line_array[3];
	#my $diff_topsis_mew = $line_array[5];
	my $diff_saw_iter_mew = $line_array[7];
	my $diff_mew_iter_mew = $line_array[9];
	my $diff_topsis_iter_mew = $line_array[11];
	my $diff_gap_mew = $line_array[13];

    if ($gen == '10')
    {
        $hash_avg2{'saw_iter'}{$chro} += $diff_saw_iter;
        $hash_avg2{'mew_iter'}{$chro} += $diff_mew_iter;
        $hash_avg2{'topsis_iter'}{$chro} += $diff_topsis_iter;
        $hash_avg2{'gap'}{$chro} += $diff_gap;

        $hash_avg_mew2{'saw_iter'}{$chro} += $diff_saw_iter_mew;
        $hash_avg_mew2{'mew_iter'}{$chro} += $diff_mew_iter_mew;
        $hash_avg_mew2{'topsis_iter'}{$chro} += $diff_topsis_iter_mew;
        $hash_avg_mew2{'gap'}{$chro} += $diff_gap_mew;
    }
	$hash_avg{$gen}{$chro} += $diff;
	$hash_avg_mew{$gen}{$chro} += $diff_mew;
	$hash_count{$gen}{$chro}++;
	
	print "$gen	$chro	$diff	$diff_mew\n";
}


close DIFF_MADM_MO;

foreach my $line (<DIFF_TOPSIS_MO>)	
{
	my @line_array = split ('\t', $line);

	my $gen = $line_array[0];
	my $chro = $line_array[1];
	my $diff = $line_array[2];
	my $diff_saw_iter = $line_array[3];
	my $diff_mew_iter = $line_array[4];
	my $diff_topsis_iter = $line_array[5];
	my $diff_gap = $line_array[6];

    if ($gen == '10')
    {
        $hash_avg_topsis2{'saw_iter'}{$chro} += $diff_saw_iter;
        $hash_avg_topsis2{'mew_iter'}{$chro} += $diff_mew_iter;
        $hash_avg_topsis2{'topsis_iter'}{$chro} += $diff_topsis_iter;
        $hash_avg_topsis2{'gap'}{$chro} += $diff_gap;
    }
	$hash_avg_topsis{$gen}{$chro} += $diff;
	$hash_count_topsis{$gen}{$chro}++;
	
	print "$gen	$chro	$diff	$diff_mew\n";
}


close DIFF_TOPSIS_MO;


my %avg = ();
my %avg_mew = ();
my %avg_topsis = ();

my %avg2 = ();
my %avg_mew2 = ();
my %avg_topsis2 = ();

foreach my $gen (sort {$a<=>$b} keys %hash_avg)
{
	print "	GEN $gen\n";
	foreach my $chro (sort {$a<=>$b} keys %{$hash_avg{$gen}})
	{
		print "		CHRO $chro\n";
		my $ratio = $hash_avg{$gen}{$chro} / $hash_count{$gen}{$chro};
		my $ratio_mew = $hash_avg_mew{$gen}{$chro} / $hash_count{$gen}{$chro};	
		my $ratio_topsis = $hash_avg_topsis{$gen}{$chro} / $hash_count_topsis{$gen}{$chro};	
		$avg{$gen}{$chro} = $ratio;
		$avg_mew{$gen}{$chro} = $ratio_mew;
		$avg_topsis{$gen}{$chro} = $ratio_topsis;

		if ($gen == '10')
		{
			my $ratio2 = $hash_avg2{'saw_iter'}{$chro} / $hash_count{'10'}{$chro};
			my $ratio_mew2 = $hash_avg_mew2{'saw_iter'}{$chro} / $hash_count{'10'}{$chro};	
			my $ratio_topsis2 = $hash_avg_topsis2{'saw_iter'}{$chro} / $hash_count_topsis{'10'}{$chro};
			$avg2{'saw_iter'}{$chro} = $ratio2;
			$avg_mew2{'saw_iter'}{$chro} = $ratio_mew2;
			$avg_topsis2{'saw_iter'}{$chro} = $ratio_topsis2;
			
            my $ratio2 = $hash_avg2{'mew_iter'}{$chro} / $hash_count{'10'}{$chro};
			my $ratio_mew2 = $hash_avg_mew2{'mew_iter'}{$chro} / $hash_count{'10'}{$chro};	
			my $ratio_topsis2 = $hash_avg_topsis2{'mew_iter'}{$chro} / $hash_count_topsis{'10'}{$chro};
			$avg2{'mew_iter'}{$chro} = $ratio2;
			$avg_mew2{'mew_iter'}{$chro} = $ratio_mew2;
			$avg_topsis2{'mew_iter'}{$chro} = $ratio_topsis2;

			my $ratio2 = $hash_avg2{'topsis_iter'}{$chro} / $hash_count{'10'}{$chro};
			my $ratio_mew2 = $hash_avg_mew2{'topsis_iter'}{$chro} / $hash_count{'10'}{$chro};	
			my $ratio_topsis2 = $hash_avg_topsis2{'topsis_iter'}{$chro} / $hash_count_topsis{'10'}{$chro};
			$avg2{'topsis_iter'}{$chro} = $ratio2;
			$avg_mew2{'topsis_iter'}{$chro} = $ratio_mew2;
			$avg_topsis2{'topsis_iter'}{$chro} = $ratio_topsis2;

			my $ratio2 = $hash_avg2{'gap'}{$chro} / $hash_count{'10'}{$chro};
			my $ratio_mew2 = $hash_avg_mew2{'gap'}{$chro} / $hash_count{'10'}{$chro};	
			my $ratio_topsis2 = $hash_avg_topsis2{'gap'}{$chro} / $hash_count_topsis{'10'}{$chro};
			$avg2{'gap'}{$chro} = $ratio2;
			$avg_mew2{'gap'}{$chro} = $ratio_mew2;
			$avg_topsis2{'gap'}{$chro} = $ratio_topsis2;
		}
	}
}

my %sum = ();
my %sum_mew = ();
my %sum_topsis = ();

my %sum2 = ();
my %sum_mew2 = ();
my %sum_topsis2 = ();

open DIFF_MADM_MO,"diff_madm_mo";

foreach my $line (<DIFF_MADM_MO>)	
{
	my @line_array = split ('\t', $line);

	my $gen = $line_array[0];
	my $chro = $line_array[1];
	my $diff = $line_array[2];
	#my $diff_topsis = $line_array[4];
	my $diff_saw_iter = $line_array[6];
	my $diff_mew_iter = $line_array[8];
	my $diff_topsis_iter = $line_array[10];
	my $diff_gap = $line_array[12];

	my $diff_mew = $line_array[3];
	#my $diff_topsis_mew = $line_array[5];
	my $diff_saw_iter_mew = $line_array[7];
	my $diff_mew_iter_mew = $line_array[9];
	my $diff_topsis_iter_mew = $line_array[11];
	my $diff_gap_mew = $line_array[13];

	$sum{$gen}{$chro} += ($diff - $avg{$gen}{$chro})**2;
	$sum_mew{$gen}{$chro} += ($diff_mew - $avg_mew{$gen}{$chro})**2;

    if ($gen == '10')
    {
		#print ($diff_saw_iter - $avg2{'saw_iter'}{$chro})**2;
	    $sum2{'saw_iter'}{$chro} += ($diff_saw_iter - $avg2{'saw_iter'}{$chro})**2;
		#print $sum2{'saw_iter'}{$chro};
	    $sum2{'mew_iter'}{$chro} += ($diff_mew_iter - $avg2{'mew_iter'}{$chro})**2;
	    $sum2{'topsis_iter'}{$chro} += ($diff_topsis_iter - $avg2{'topsis_iter'}{$chro})**2;
	    $sum2{'gap'}{$chro} += ($diff_gap - $avg2{'gap'}{$chro})**2;
	    $sum_mew2{'saw_iter'}{$chro} += ($diff_saw_iter_mew - $avg_mew2{'saw_iter'}{$chro})**2;
	    $sum_mew2{'mew_iter'}{$chro} += ($diff_mew_iter_mew - $avg_mew2{'mew_iter'}{$chro})**2;
	    $sum_mew2{'topsis_iter'}{$chro} += ($diff_topsis_iter_mew - $avg_mew2{'topsis_iter'}{$chro})**2;
	    $sum_mew2{'gap'}{$chro} += ($diff_gap_mew - $avg_mew2{'gap'}{$chro})**2;
    }
}

close DIFF_MADM_MO;

open DIFF_TOPSIS_MO,"diff_madm_mo";

foreach my $line (<DIFF_TOPSIS_MO>)	
{
	my @line_array = split ('\t', $line);

	my $gen = $line_array[0];
	my $chro = $line_array[1];
	my $diff = $line_array[2];
	my $diff_saw_iter = $line_array[3];
	my $diff_mew_iter = $line_array[4];
	my $diff_topsis_iter = $line_array[5];
	my $diff_gap = $line_array[6];

	$sum_topsis{$gen}{$chro} += ($diff - $avg_topsis{$gen}{$chro})**2;

    if ($gen == '10')
    {
	    $sum_topsis2{'saw_iter'}{$chro} += ($diff_saw_iter - $avg_topsis2{'saw_iter'}{$chro})**2;
	    $sum_topsis2{'mew_iter'}{$chro} += ($diff_mew_iter - $avg_topsis2{'mew_iter'}{$chro})**2;
	    $sum_topsis2{'topsis_iter'}{$chro} += ($diff_topsis_iter - $avg_topsis2{'topsis_iter'}{$chro})**2;
	    $sum_topsis2{'gap'}{$chro} += ($diff_gap - $avg_topsis2{'gap'}{$chro})**2;
    }
}

close DIFF_TOPSIS_MO;


foreach my $gen (sort {$a<=>$b} keys %sum)
{
	open AVG, ">avg_diff_$gen";

	foreach my $chro (sort {$a<=>$b} keys %{$sum{$gen}})
	{
		my $ratio = ($sum{$gen}{$chro} / $hash_count{$gen}{$chro})**0.5;
		my $ratio_mew = ($sum_mew{$gen}{$chro} / $hash_count{$gen}{$chro})**0.5;

        my $ratio2 = ($sum2{'saw_iter'}{$chro} / $hash_count{'10'}{$chro})**0.5;
        my $ratio3 = ($sum2{'mew_iter'}{$chro} / $hash_count{'10'}{$chro})**0.5;
        my $ratio4 = ($sum2{'topsis_iter'}{$chro} / $hash_count{'10'}{$chro})**0.5;
        my $ratio5 = ($sum2{'gap'}{$chro} / $hash_count{'10'}{$chro})**0.5;
	    my $ratio_mew2 = ($sum_mew2{'saw_iter'}{$chro} / $hash_count{'10'}{$chro})**0.5;
	    my $ratio_mew3 = ($sum_mew2{'mew_iter'}{$chro} / $hash_count{'10'}{$chro})**0.5;
	    my $ratio_mew4 = ($sum_mew2{'topsis_iter'}{$chro} / $hash_count{'10'}{$chro})**0.5;
	    my $ratio_mew5 = ($sum_mew2{'gap'}{$chro} / $hash_count{'10'}{$chro})**0.5;

		if ($chro%2 == 0)
		{
            my $nb_apps = $chro/2;
			print AVG "$gen	$nb_apps	$avg{$gen}{$chro}	$ratio	$avg_mew{$gen}{$chro}	$ratio_mew";
            if ($gen == '10')
            {
                print AVG "	$avg2{'saw_iter'}{$chro}	$ratio2	$avg_mew2{'saw_iter'}{$chro}	$ratio_mew2	$avg2{'mew_iter'}{$chro}	$ratio3	$avg_mew2{'mew_iter'}{$chro}	$ratio_mew3	$avg2{'topsis_iter'}{$chro}	$ratio4	$avg_mew2{'topsis_iter'}{$chro}	$ratio_mew4	$avg2{'gap'}{$chro}	$ratio5	$avg_mew2{'gap'}{$chro}	$ratio_mew5";
            }
            print AVG "\n";
		}
	}
	close AVG;
}

foreach my $gen (sort {$a<=>$b} keys %sum)
{
	open AVG_TOPSIS, ">avg_diff_topsis_$gen";

	foreach my $chro (sort {$a<=>$b} keys %{$sum_topsis{$gen}})
	{
		my $ratio = ($sum_topsis{$gen}{$chro} / $hash_count_topsis{$gen}{$chro})**0.5;

        my $ratio2 = ($sum_topsis2{'saw_iter'}{$chro} / $hash_count_topsis{'10'}{$chro})**0.5;
        my $ratio3 = ($sum_topsis2{'mew_iter'}{$chro} / $hash_count_topsis{'10'}{$chro})**0.5;
        my $ratio4 = ($sum_topsis2{'topsis_iter'}{$chro} / $hash_count_topsis{'10'}{$chro})**0.5;
        my $ratio5 = ($sum_topsis2{'gap'}{$chro} / $hash_count_topsis{'10'}{$chro})**0.5;

		if ($chro%2 == 0)
		{
            my $nb_apps = $chro/2;
			print AVG_TOPSIS "$gen	$nb_apps	$avg_topsis{$gen}{$chro}	$ratio";
            if ($gen == '10')
            {
                print AVG_TOPSIS "	$avg_topsis2{'saw_iter'}{$chro}	$ratio2	$avg_topsis2{'mew_iter'}{$chro}	$ratio3	$avg_topsis2{'topsis_iter'}{$chro}	$ratio4	$avg_topsis2{'gap'}{$chro}	$ratio5";
            }
            print AVG_TOPSIS "\n";
		}
	}
	close AVG_TOPSIS;
}

open PLOT_AVG, ">plot_diff_avg.gnuplot";

print PLOT_AVG "set terminal postscript color enhanced\n";
print PLOT_AVG "set output \"plot_diff_avg_saw.eps\"\n";
print PLOT_AVG "set xlabel \"Number of Applications\"\n";
print PLOT_AVG "set ylabel \"D(GA,SAW), D(new algths,SAW)\"\n";
print PLOT_AVG "set colorsequence classic\n";
print PLOT_AVG "set key below\n";
print PLOT_AVG "set grid\n";
print PLOT_AVG "set xrange [0:13]\n";
print PLOT_AVG "plot \"avg_diff_10\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=10\", \"avg_diff_25\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=25\", \"avg_diff_50\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=50\", \"avg_diff_100\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=100\", \"avg_diff_250\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=250\", \"avg_diff_10\" u 2:7 with points pt 9 ps 1.5 lc 9 lw 3  title \"SAW p/ APP\", \"avg_diff_10\" u 2:11 with points pt 10 ps 1.5 lc 9 lw 3  title \"MEW p/ APP\", \"avg_diff_10\" u 2:15 with points pt 11 ps 1.5 lc 9 lw 3  title \"TOPSIS p/ APP\", \"avg_diff_10\" u 2:19 with points pt 12 ps 1.5 lc 5 lw 3  title \"GAP\"";

close PLOT_AVG;

open PLOT_AVG, ">plot_diff_avg_mew.gnuplot";

print PLOT_AVG "set terminal postscript color enhanced\n";
print PLOT_AVG "set output \"plot_diff_avg_mew.eps\"\n";
print PLOT_AVG "set xlabel \"Number of applications\"\n";
print PLOT_AVG "set ylabel \"D(GA,MEW), D(new algths,MEW)\"\n";
print PLOT_AVG "set colorsequence classic\n";
print PLOT_AVG "set key below\n";
print PLOT_AVG "set grid\n";
print PLOT_AVG "set xrange [0:13]\n";
print PLOT_AVG "plot \"avg_diff_10\" u 2:5 with points ps 1.5 lc 1 lw 3  title \"g=10 MEW\", \"avg_diff_25\" u 2:5 with points ps 1.5 lc 1 lw 3  title \"g=25 MEW\", \"avg_diff_50\" u 2:5 with points ps 1.5 lc 1 lw 3  title \"g=50 MEW\", \"avg_diff_100\" u 2:5 with points ps 1.5 lc 1 lw 3  title \"g=100 MEW\", \"avg_diff_250\" u 2:5 with points ps 1.5 lc 1 lw 3  title \"g=250 MEW\", \"avg_diff_10\" u 2:9 with points pt 9 ps 1.5 lc 9 lw 3  title \"SAW p/ APP MEW\", \"avg_diff_10\" u 2:13 with points pt 10 ps 1.5 lc 9 lw 3  title \"MEW p/APP MEW\", \"avg_diff_10\" u 2:17 with points pt 11 ps 1.5 lc 9 lw 3  title \"TOPSIS p/ APP MEW\", \"avg_diff_10\" u 2:21 with points pt 12 ps 1.5 lc 5 lw 3  title \"GAP MEW\"";

close PLOT_AVG;

open PLOT_AVG, ">plot_diff_avg_topsis.gnuplot";

print PLOT_AVG "set terminal postscript color enhanced\n";
print PLOT_AVG "set output \"plot_diff_avg_topsis.eps\"\n";
print PLOT_AVG "set xlabel \"Number of applications\"\n";
print PLOT_AVG "set ylabel \"D(GA,TOPSIS), D(new algths,TOPSIS)\"\n";
print PLOT_AVG "set colorsequence classic\n";
print PLOT_AVG "set key below\n";
print PLOT_AVG "set grid\n";
print PLOT_AVG "set xrange [0:13]\n";
print PLOT_AVG "plot \"avg_diff_topsis_10\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=10 TOPSIS\", \"avg_diff_topsis_25\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=25 TOPSIS\", \"avg_diff_topsis_50\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=50 TOPSIS\", \"avg_diff_topsis_100\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=100 TOPSIS\", \"avg_diff_topsis_250\" u 2:3 with points ps 1.5 lc 1 lw 3  title \"g=250 TOPSIS\", \"avg_diff_topsis_10\" u 2:5 with points pt 9 ps 1.5 lc 9 lw 3  title \"SAW p/ APP TOPSIS\", \"avg_diff_topsis_10\" u 2:7 with points pt 10 ps 1.5 lc 9 lw 3  title \"MEW p/ APP TOPSIS\", \"avg_diff_topsis_10\" u 2:9 with points pt 11 ps 1.5 lc 9 lw 3  title \"TOPSIS p/ APP TOPSIS\", \"avg_diff_topsis_10\" u 2:11 with points pt 12 ps 1.5 lc 5 lw 3  title \"GAP TOPSIS\"";

close PLOT_AVG;

my $result = `gnuplot plot_diff_avg.gnuplot`;
my $result = `gnuplot plot_diff_avg_mew.gnuplot`;
my $result = `gnuplot plot_diff_avg_topsis.gnuplot`;
