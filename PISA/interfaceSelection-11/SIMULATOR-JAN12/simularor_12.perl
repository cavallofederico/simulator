#!/usr/bin/perl

use POSIX;
use strict;
#******** SIMULATOR FOR PISA FRAMEWORK
######### Generation of ramdom scenarios to evaluate the evolution of the objectives values
######### Comparison with single-objective strategies
#********

#Arguments

#my $gen = $ARGV[0];
#srand($ARGV[1]); 

my @generations = (10,25,50,75,100,250);
#my @generations = (2,10,25,50);

#General random variables
my $min_ifaces = 1;
my $max_ifaces = 5;
my $min_apps = 1;
my $max_apps = 30;
my $winSize = 20;
my $nb_bursts = 3;
my $first_sim = 1;
my $first_sim_all = 1;


my $nb_ifaces = int(rand($max_ifaces-$min_ifaces)) + $min_ifaces;
my $nb_apps = int(rand($max_apps - $min_apps)) + $min_apps;

my %flows = ();
my %ifaces = ();
my %ifaces_energy = ();
my %ifaces_timers = ();
my %ifaces_type = ();

my %events = ();
my %events_id = ();

my $min_pow1 = 600; 
my $max_pow1 = 1200;
my $min_pow2 = 200;
my $max_pow2 = 500;
my $min_pow3 = 10;
my $max_pow3 = 80;
my $min_timer1 = 5;
my $max_timer1 = 6;
my $min_timer2 = 4;
my $max_timer2 = 12;

# Type 0->360 1->480 2->720 3->1080
my @video_bw = (628, 928, 2095, 3052); 


my $energy_er = 0;
my $bdw_er = 0;

my $win_tot = 0; # combien de fois on fait mieux que ER
my $loose_tot = 0;
my $ind_tot = 0;

my %delay_calc = ();
my %count_calc = ();

my @pareto_set = ();

my $time_all_global;
my $time_madm_global;
my $time_madm_2_global;
my $time_gap_global;

#variables for reallocation

my %hash_new_mew = ();
my %hash_new_saw = ();
my %hash_new_ga = ();
my %hash_new_topsis = ();
my %hash_new_saw_iter = ();
my %hash_new_mew_iter = ();
my %hash_new_topsis_iter = ();
my %hash_new_gap = ();

my %hash_old_saw = ();
my %hash_old_mew = ();
my %hash_old_ga = ();
my %hash_old_topsis = ();
my %hash_old_saw_iter = ();
my %hash_old_mew_iter = ();
my %hash_old_topsis_iter = ();
my %hash_old_gap = ();

my @array_bs_saw = ();
my @array_bs_mew = ();
my @array_bs_ga = ();
my @array_bs_topsis = ();
my @array_bs_saw_iter = ();
my @array_bs_mew_iter = ();
my @array_bs_topsis_iter = ();
my @array_bs_gap = ();
my @min_bs_mew=();
my @min_bs_saw=();
my @min_bs_mo=();
my @min_bs_topsis=();
my @min_bs_saw_iter=();
my @min_bs_mew_iter=();
my @min_bs_topsis_iter=();
my @min_bs_gap=();

my $update_realloc = 0;
my $update_realloc_all = 0;

my %hash_new_saw_all = ();
my %hash_new_mew_all = ();
my %hash_new_ga_all = ();
my %hash_new_topsis_all = ();
my %hash_new_saw_iter_all = ();
my %hash_new_mew_iter_all = ();
my %hash_new_topsis_iter_all = ();
my %hash_new_gap_all = ();
my %hash_old_saw_all = ();
my %hash_old_mew_all = ();
my %hash_old_ga_all = ();
my %hash_old_topsis_all = ();
my %hash_old_saw_iter_all = ();
my %hash_old_mew_iter_all = ();
my %hash_old_topsis_iter_all = ();
my %hash_old_gap_all = ();

my $madm_bs;
my $mew_bs;
my $mo_bs;
my $topsis_bs;
my $saw_iter_bs;
my $mew_iter_bs;
my $topsis_iter_bs;
my $gap_bs;

my @app_assign_id = ();

my %realloc_saw = ();
my %realloc_mew = ();
my %realloc_ga = ();
my %realloc_topsis = ();
my %realloc_saw_iter = ();
my %realloc_mew_iter = ();
my %realloc_topsis_iter = ();
my %realloc_gap = ();

my %realloc_saw_all = ();
my %realloc_mew_all = ();
my %realloc_ga_all = ();
my %realloc_topsis_all = ();
my %realloc_saw_iter_all = ();
my %realloc_mew_iter_all = ();
my %realloc_topsis_iter_all = ();
my %realloc_gap_all = ();

my %realloc_all = ();

open OUT_REALLOC,">out_realloc.txt";
open OUT_REALLOC_ALL,">out_realloc_all.txt";

#Geometric random generator
sub rand_geom() # p
{
    return  (log(1-rand()) / log(1-$_[0]) );
}


#Pareto random generator
sub rand_pareto() # scale, shape
{
    return  ( $_[0] / ( (1-rand()) )**(1/$_[1]) );
}

#Exponential random generator
sub rand_exp() # lambda
{
    return  ( -log(rand()) / $_[0] );
}


sub poisson() #lamda, n
{

    my $n = $_[0];
    my $lambda = $_[1];
    my $offset = 0;

    print "LAMBDA = $lambda N = $n\n";

    my @sample = ();
    $sample[0] = 0;

    for (my $i = 1; $i < $n; $i++)
    {
        my $exp = &rand_exp($lambda);
        #print "EXP = $exp\n";
        $offset += $exp;

        $sample[$i] = $offset;      
    }

    print "SAMPLES = @sample\n";
    return(@sample);
    
}

sub not_in_set()
{
    my $value = $_[0];

    #print "VALUE $value\n";

    foreach my $key (@pareto_set)
    {
        #print "KEY $key";
        if ($key eq $value)
        {
            #print "FOUND!\n";
            return 1;
        }
    }

    return 0;
}

#Generateur de flux HTTP
sub gen_http()
{
    my $id = $_[0];
    my $time_offset = $_[1];

    my $nb_reqs = int( &rand_geom(1/5) ) + 2; #From Yeh paper ~GEOM(1/5)    1/5
    #print "ID $id  CALLS $nb_reqs\n";
    
    my $time_init = $time_offset;

    for (my $i = 0; $i < $nb_reqs; $i++)
    {
        if ($i == 0)
        {
            my $size = &rand_pareto(81.5,1.1); #From Yeh paper
            my $packets = &rand_geom(1/25) + 2; #From Yeh paper
            $flows{$id}{$time_init} = $size*$packets;
            $events{$time_init} = "NEW_FLOW";
            $events_id{$time_init} = $id;

        } else {

            my $time = &rand_geom(1/12); #From ETSI model 1/12
            $time_init += $time;

            my $size = &rand_pareto(81.5,1.1); #From Yeh paper
            my $packets = &rand_geom(1/25); #From Yeh paper

            $flows{$id}{$time_init} = $size*$packets;
        }
    }
}


#Generateur de flux REAL-TIME
sub gen_rt()
{
    my $id = $_[0];
    my $type = $_[1];
    my $time_offset = $_[2];
    
    my $nb_flux = int( &rand_geom(1/10) ) + 2; #1/5
    #print "ID $id  FLOWS $nb_flux\n";

    my $time_init = $time_offset;

    my $size = 0;
    my $on_time = 0;
    my $off_time = 0;

    for (my $i = 0; $i < $nb_flux; $i++)
    {
        #print "FOR\n";
        if ($i == 0)
        {
            $on_time = &rand_geom(1/60); #From Yeh paper 1/30  1/60
            $off_time = &rand_geom(1/120); #From Yeh paper 1/120
            $size = $on_time * $video_bw[$type]*1024/8; #in bytes/s

            $flows{$id}{$time_init} = $size;
            $events{$time_init} = "NEW_FLOW";
            $events_id{$time_init} = $id;           

        } else {

            $time_init += $on_time + $off_time;
            $size = $on_time * $video_bw[$type] * 1024 / 8; #in bytes/s

            #next
            $on_time = &rand_geom(1/60); #From Yeh paper
            $off_time = &rand_geom(1/120); #From Yeh paper

            #print "ID $id  TIME $time_init\n";
            $flows{$id}{$time_init} = $size;
        }
    }
}


#Generateur de flux BACKGROUND
sub gen_bkg()
{

}

sub gen_exp()
{

}

sub number_reallocs2()
{}

sub number_reallocs ()
{

    if ($update_realloc == 1)
    {

        my $index = 0;
    
        my $event = $_[0];
        my $gene = $_[1];

        print OUT_REALLOC "EVENT $event\n";
    

        if ($first_sim == 0)
        {
            #comparer reallocation dès la deuxième assignation  
            #@split_saw = split(undef,$min_bs_saw);
            #@split_mew = split(undef,$min_bs_mew);
            #@split_ga = split(undef,$min_bs_mo);

            %{$hash_new_saw{$gene}} = ();
            %{$hash_new_mew{$gene}} = ();
            %{$hash_new_topsis{$gene}} = ();
            %{$hash_new_saw_iter{$gene}} = ();
            %{$hash_new_mew_iter{$gene}} = ();
            %{$hash_new_topsis_iter{$gene}} = ();
            %{$hash_new_gap{$gene}} = ();
            %{$hash_new_ga{$gene}} = ();
            

            foreach my $app_ass (@app_assign_id)
            {
                $hash_new_saw{$gene}{$app_ass} = $min_bs_saw[$index];
                $hash_new_mew{$gene}{$app_ass} = $min_bs_mew[$index];
                $hash_new_topsis{$gene}{$app_ass} = $min_bs_mew[$index];
                $hash_new_mew_iter{$gene}{$app_ass} = $min_bs_mew[$index];
                $hash_new_saw_iter{$gene}{$app_ass} = $min_bs_mew[$index];
                $hash_new_topsis_iter{$gene}{$app_ass} = $min_bs_mew[$index];
                $hash_new_gap{$gene}{$app_ass} = $min_bs_mew[$index];
                $hash_new_ga{$gene}{$app_ass} = $min_bs_mo[$index];

                $index ++;
            }



            %{$hash_new_saw_all{$gene}} = ();
            %{$hash_new_mew_all{$gene}} = ();
            %{$hash_new_topsis_all{$gene}} = ();
            %{$hash_new_mew_iter_all{$gene}} = ();
            %{$hash_new_saw_iter_all{$gene}} = ();
            %{$hash_new_topsis_iter_all{$gene}} = ();
            %{$hash_new_gap_all{$gene}} = ();
            %{$hash_new_ga_all{$gene}} = ();

            foreach my $pos (@array_bs_saw)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_saw_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }


            foreach my $pos (@array_bs_mew)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_mew_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }

            foreach my $pos (@array_bs_topsis)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_topsis_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }

            foreach my $pos (@array_bs_saw_iter)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_saw_iter_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }

            foreach my $pos (@array_bs_mew_iter)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_mew_iter_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }

            foreach my $pos (@array_bs_topsis_iter)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_topsis_iter_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }


            foreach my $pos (@array_bs_gap)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_gap_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }
            foreach my $pos (@array_bs_ga)
            {
                my @array_split = split(undef,$pos);
            
                $index=0;
                
                foreach my $app_ass (@app_assign_id)
                {
                    $hash_new_ga_all{$gene}{$app_ass}{$pos} = $array_split[$index];
                    $index++;
                }

            }

            print "OLD\n"; print OUT_REALLOC "OLD\n";
            print " SAW: "; print OUT_REALLOC " SAW: ";
            foreach my $key (keys %{$hash_old_saw{$gene}})
            {
                print "$key=>$hash_old_saw{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_saw{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " MEW: "; print OUT_REALLOC " MEW: ";
            foreach my $key (keys %{$hash_old_mew{$gene}})
            {
                print "$key=>$hash_old_mew{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_mew{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " TOPSIS: "; print OUT_REALLOC " TOPSIS: ";
            foreach my $key (keys %{$hash_old_topsis{$gene}})
            {
                print "$key=>$hash_old_topsis{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_topsis{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " SAW_ITER: "; print OUT_REALLOC " SAW_ITER: ";
            foreach my $key (keys %{$hash_old_saw_iter{$gene}})
            {
                print "$key=>$hash_old_saw_iter{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_saw_iter{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " MEW_ITER: "; print OUT_REALLOC " MEW_ITER: ";
            foreach my $key (keys %{$hash_old_mew_iter{$gene}})
            {
                print "$key=>$hash_old_mew_iter{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_mew_iter{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " TOPSIS_ITER: "; print OUT_REALLOC " TOPSIS_ITER: ";
            foreach my $key (keys %{$hash_old_topsis_iter{$gene}})
            {
                print "$key=>$hash_old_topsis_iter{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_topsis_iter{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " GAP: "; print OUT_REALLOC " GAP: ";
            foreach my $key (keys %{$hash_old_gap{$gene}})
            {
                print "$key=>$hash_old_gap{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_gap{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " GA: "; print OUT_REALLOC "  GA: ";
            foreach my $key (keys %{$hash_old_ga{$gene}})
            {
                print "$key=>$hash_old_ga{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_old_ga{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print "NEW\n"; print OUT_REALLOC "NEW\n";
            print " SAW: "; print OUT_REALLOC " SAW: ";
            foreach my $key (keys %{$hash_new_saw{$gene}})
            {
                print "$key=>$hash_new_saw{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_saw{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " MEW: "; print OUT_REALLOC " MEW: ";
            foreach my $key (keys %{$hash_new_mew{$gene}})
            {
                print "$key=>$hash_new_mew{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_mew{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " TOPSIS: "; print OUT_REALLOC " TOPSIS: ";
            foreach my $key (keys %{$hash_new_topsis{$gene}})
            {
                print "$key=>$hash_new_topsis{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_topsis{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " SAW_ITER: "; print OUT_REALLOC " SAW_ITER: ";
            foreach my $key (keys %{$hash_new_saw_iter{$gene}})
            {
                print "$key=>$hash_new_saw_iter{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_saw_iter{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " MEW_ITER: "; print OUT_REALLOC " MEW_ITER: ";
            foreach my $key (keys %{$hash_new_mew_iter{$gene}})
            {
                print "$key=>$hash_new_mew_iter{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_mew_iter{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " TOPSIS_ITER: "; print OUT_REALLOC " TOPSIS_ITER: ";
            foreach my $key (keys %{$hash_new_topsis_iter{$gene}})
            {
                print "$key=>$hash_new_topsis_iter{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_topsis_iter{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " GAP: "; print OUT_REALLOC " GAP: ";
            foreach my $key (keys %{$hash_new_gap{$gene}})
            {
                print "$key=>$hash_new_gap{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_gap{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";
            print " GA: "; print OUT_REALLOC "  GA: ";
            foreach my $key (keys %{$hash_new_ga{$gene}})
            {
                print "$key=>$hash_new_ga{$gene}{$key} "; print OUT_REALLOC "$key=>$hash_new_ga{$gene}{$key} ";
            }
            print "\n"; print OUT_REALLOC "\n";


            foreach my $key1 (keys %{$hash_new_saw{$gene}})
            {
                #foreach my $key2 (%hash_old_saw)
                if (exists $hash_old_saw{$gene}{$key1})             
                {
                    #if (($key1 == $key2) && ($key1 >=0) && ($key2 >=0))
                    #{
                        print "SAW KEY NEW $key1    KEY OLD: $key1\n"; print OUT_REALLOC "SAW KEY NEW $key1 KEY OLD: $key1\n";
                        print " SAW HASH KEY NEW $hash_new_saw{$gene}{$key1}    HASH KEY OLD: $hash_old_saw{$gene}{$key1}\n"; print OUT_REALLOC "   SAW HASH KEY NEW $hash_new_saw{$gene}{$key1}    HASH KEY OLD: $hash_old_saw{$gene}{$key1}\n";

                        if ($hash_new_saw{$gene}{$key1} != $hash_old_saw{$gene}{$key1})
                        {
                            $realloc_saw{$gene} ++;
                            print "     REALLOC SAW\n"; print OUT_REALLOC "     REALLOC SAW\n";
                        }
                    #}
                }
            }

            foreach my $key1 (keys %{$hash_new_mew{$gene}})
            {
                #foreach my $key2 (%hash_old_mew)
                if (exists $hash_old_mew{$gene}{$key1})                         
                {
                    #if (($key1 == $key2) && ($key1 >=0) && ($key2 >=0))
                    #{
                        print "MEW KEY NEW $key1    KEY OLD: $key1\n"; print OUT_REALLOC "MEW KEY NEW $key1 KEY OLD: $key1\n";
                        print " MEW HASH KEY NEW $hash_new_mew{$gene}{$key1}    HASH KEY OLD: $hash_old_mew{$gene}{$key1}\n"; print OUT_REALLOC "   MEW HASH KEY NEW $hash_new_mew{$gene}{$key1}    HASH KEY OLD: $hash_old_mew{$gene}{$key1}\n";

                        if ($hash_new_mew{$gene}{$key1} != $hash_old_mew{$gene}{$key1})
                        {
                            $realloc_mew{$gene} ++;
                            print "     REALLOC MEW\n"; print OUT_REALLOC "     REALLOC MEW\n";
                        }

                    #}
                }
            }

            foreach my $key1 (keys %{$hash_new_ga{$gene}})
            {
                #foreach my $key2 (%hash_old_ga)
                if (exists $hash_old_ga{$gene}{$key1})              
                {
                    #if (($key1 == $key2) && ($key1 >=0) && ($key2 >=0))
                    #{
                        print "GA KEY NEW $key1 KEY OLD: $key1\n"; print OUT_REALLOC "GA KEY NEW $key1  KEY OLD: $key1\n";
                        print " GA HASH KEY NEW $hash_new_ga{$gene}{$key1}  HASH KEY OLD: $hash_old_ga{$gene}{$key1}\n"; print OUT_REALLOC "    GA HASH KEY NEW $hash_new_ga{$gene}{$key1}  HASH KEY OLD: $hash_old_ga{$gene}{$key1}\n";

                        if ($hash_new_ga{$gene}{$key1} != $hash_old_ga{$gene}{$key1})
                        {
                            $realloc_ga{$gene} ++;
                            print "     REALLOC GA\n"; print OUT_REALLOC "      REALLOC GA\n";
                        }

                    #}
                }
            }


            ##### hash ALL

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_saw_all{$gene}})
            {
                print OUT_REALLOC_ALL "SAW KEY NEW $key1\n";

                if (exists $hash_old_saw{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " SAW KEY OLD $key1=>$hash_old_saw{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_saw_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     SAW BS ITEM $bs_item\n";

                        if ($hash_new_saw_all{$gene}{$key1}{$bs_item} != $hash_old_saw{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_saw_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            { 
                $realloc_saw_all{$gene}+=$min_all; 
            } else
            {
                $realloc_saw_all{$gene} = 0;
            }




            # MEW

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_mew_all{$gene}})
            {
                print OUT_REALLOC_ALL "MEW KEY NEW $key1\n";

                if (exists $hash_old_mew{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " MEW KEY OLD $key1=>$hash_old_mew{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_mew_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     MEW BS ITEM $bs_item\n";

                        if ($hash_new_mew_all{$gene}{$key1}{$bs_item} != $hash_old_mew{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_mew_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            {
                $realloc_mew_all{$gene}+=$min_all;
            } else
            {
                $realloc_mew_all{$gene} = 0;
            }

            # TOPSIS

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_topsis_all{$gene}})
            {
                print OUT_REALLOC_ALL "TOPSIS KEY NEW $key1\n";

                if (exists $hash_old_topsis{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " TOPSIS KEY OLD $key1=>$hash_old_topsis{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_topsis_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     TOPSIS BS ITEM $bs_item\n";

                        if ($hash_new_topsis_all{$gene}{$key1}{$bs_item} != $hash_old_topsis{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_topsis_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            { 
                $realloc_topsis_all{$gene}+=$min_all; 
            } else
            {
                $realloc_topsis_all{$gene} = 0;
            }

            # SAW_ITER

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_saw_iter_all{$gene}})
            {
                print OUT_REALLOC_ALL "SAW_ITER KEY NEW $key1\n";

                if (exists $hash_old_saw_iter{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " SAW_ITER KEY OLD $key1=>$hash_old_saw_iter{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_saw_iter_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     SAW_ITER BS ITEM $bs_item\n";

                        if ($hash_new_saw_iter_all{$gene}{$key1}{$bs_item} != $hash_old_saw_iter{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_saw_iter_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            { 
                $realloc_saw_iter_all{$gene}+=$min_all; 
            } else
            {
                $realloc_saw_iter_all{$gene} = 0;
            }



            # MEW_ITER

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_mew_iter_all{$gene}})
            {
                print OUT_REALLOC_ALL "MEW_ITER KEY NEW $key1\n";

                if (exists $hash_old_mew_iter{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " MEW_ITER KEY OLD $key1=>$hash_old_mew_iter{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_mew_iter_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     MEW_ITER BS ITEM $bs_item\n";

                        if ($hash_new_mew_iter_all{$gene}{$key1}{$bs_item} != $hash_old_mew_iter{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_mew_iter_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            { 
                $realloc_mew_iter_all{$gene}+=$min_all; 
            } else
            {
                $realloc_mew_iter_all{$gene} = 0;
            }




            # TOPSIS_ITER

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_topsis_iter_all{$gene}})
            {
                print OUT_REALLOC_ALL "TOPSIS_ITER KEY NEW $key1\n";

                if (exists $hash_old_topsis_iter{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " TOPSIS_ITER KEY OLD $key1=>$hash_old_topsis_iter{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_topsis_iter_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     TOPSIS_ITER BS ITEM $bs_item\n";

                        if ($hash_new_topsis_iter_all{$gene}{$key1}{$bs_item} != $hash_old_topsis_iter{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_topsis_iter_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            { 
                $realloc_topsis_iter_all{$gene}+=$min_all; 
            } else
            {
                $realloc_topsis_iter_all{$gene} = 0;
            }


            # GAP

            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_gap_all{$gene}})
            {
                print OUT_REALLOC_ALL "GAP KEY NEW $key1\n";

                if (exists $hash_old_gap{$gene}{$key1})             
                {
                    print OUT_REALLOC_ALL " GAP KEY OLD $key1=>$hash_old_gap{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_gap_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     GAP BS ITEM $bs_item\n";

                        if ($hash_new_gap_all{$gene}{$key1}{$bs_item} != $hash_old_gap{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_gap_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            { 
                $realloc_gap_all{$gene}+=$min_all; 
            } else
            {
                $realloc_gap_all{$gene} = 0;
            }







            #GA
            %realloc_all = ();

            foreach my $key1 (keys %{$hash_new_ga_all{$gene}})
            {
                print OUT_REALLOC_ALL "GA KEY NEW $key1\n";

                if (exists $hash_old_ga{$gene}{$key1})              
                {
                    print OUT_REALLOC_ALL " GA KEY OLD $key1=>$hash_old_ga{$gene}{$key1}\n";

                    foreach my $bs_item (keys %{$hash_new_ga_all{$gene}{$key1}})
                    {
                        print OUT_REALLOC_ALL "     GA BS ITEM $bs_item\n";

                        if ($hash_new_ga_all{$gene}{$key1}{$bs_item} != $hash_old_ga{$gene}{$key1})
                        {
                            print OUT_REALLOC_ALL "         $key1=>$hash_new_ga_all{$gene}{$key1}{$bs_item} --> REALLOC\n";
                            $realloc_all{$bs_item}++;
                        } else 
                        {
                            if (!exists $realloc_all{$bs_item})
                            {
                                $realloc_all{$bs_item} = 0;
                            }
                        }
                    }
                }
            }

            my $min_all = 999999;

            foreach my $bs_item (keys %realloc_all)
            {
                if (($realloc_all{$bs_item} < $min_all)&&(exists $realloc_all{$bs_item}))
                {
                    $min_all = $realloc_all{$bs_item}
                }
            }

            if ($min_all != 999999)
            {
                $realloc_ga_all{$gene}+=$min_all;
            } else
            {
                $realloc_ga_all{$gene} = 0;
            }


            print REALLOCS "$event\t$gene\t";

            if (exists $realloc_saw{$gene})
            {
                print REALLOCS "$realloc_saw{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }           

            if (exists $realloc_mew{$gene})
            {
                print REALLOCS "$realloc_mew{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }

            if (exists $realloc_topsis{$gene})
            {
                print REALLOCS "$realloc_topsis{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }

            if (exists $realloc_saw_iter{$gene})
            {
                print REALLOCS "$realloc_saw_iter{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }

            if (exists $realloc_mew_iter{$gene})
            {
                print REALLOCS "$realloc_mew_iter{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }

            if (exists $realloc_topsis_iter{$gene})
            {
                print REALLOCS "$realloc_mew{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }

            if (exists $realloc_gap{$gene})
            {
                print REALLOCS "$realloc_mew{$gene}\t";       
            }else
            {
                print REALLOCS "0\t";
            }

            if (exists $realloc_ga{$gene})
            {
                print REALLOCS "$realloc_ga{$gene}\n";       
            }else
            {
                print REALLOCS "0\n";
            }


            print REALLOCS_ALL "$event\t$gene\t";

            if (exists $realloc_saw_all{$gene})
            {
                print REALLOCS_ALL "$realloc_saw_all{$gene}\t$realloc_mew_all{$gene}\t$realloc_topsis_all{$gene}\t$realloc_saw_iter_all{$gene}\t$realloc_mew_iter_all{$gene}\t$realloc_topsis_iter_all{$gene}\t$realloc_gap_all{$gene}\t$realloc_ga_all{$gene}\n";       
            }else
            {
                print REALLOCS_ALL "0\n";
            }           

            

            #print REALLOCS "$event $gene   $realloc_saw{$gene} $realloc_mew{$gene} $realloc_ga{$gene}\n";
            print "REALLOCS $event\t$gene\t$realloc_saw{$gene}\t$realloc_mew{$gene}\t$realloc_topsis{$gene}\t$realloc_saw_iter{$gene}\t$realloc_mew_iter{$gene}\t$realloc_topsis_iter{$gene}\t$realloc_gap{$gene}\t$realloc_ga{$gene}\n"; 
            print OUT_REALLOC "REALLOCS $event\t$gene\t$realloc_saw{$gene}\t$realloc_mew{$gene}\t$realloc_topsis{$gene}\t$realloc_saw_iter{$gene}\t$realloc_mew_iter{$gene}\t$realloc_topsis_iter{$gene}\t$realloc_gap{$gene}\t$realloc_ga{$gene}\n";


        }

        #garder la solution minimale précedante
    
        $index = 0;

        %{$hash_old_saw{$gene}} = ();
        %{$hash_old_mew{$gene}} = ();
        %{$hash_old_topsis{$gene}} = ();
        %{$hash_old_saw_iter{$gene}} = ();
        %{$hash_old_mew_iter{$gene}} = ();
        %{$hash_old_topsis_iter{$gene}} = ();
        %{$hash_old_gap{$gene}} = ();
        %{$hash_old_ga{$gene}} = ();

        foreach my $app_ass (@app_assign_id)
        {
            print "APP_ASS  $app_ass\n";
            print " SAW $min_bs_saw[$index]\n";
            print " MEW $min_bs_mew[$index]\n";
            print " TOPSIS $min_bs_topsis[$index]\n";
            print " SAW_ITER $min_bs_saw_iter[$index]\n";
            print " MEW_ITER $min_bs_mew_iter[$index]\n";
            print " TOPSIS_ITER $min_bs_topsis_iter[$index]\n";
            print " GAP $min_bs_gap[$index]\n";
            print " GA $min_bs_mo[$index]\n";

            print OUT_REALLOC "APP_ASS  $app_ass\n";
            print OUT_REALLOC " SAW $min_bs_saw[$index]\n";
            print OUT_REALLOC " MEW $min_bs_mew[$index]\n";
            print OUT_REALLOC " TOPSIS $min_bs_topsis[$index]\n";
            print OUT_REALLOC " SAW_ITER $min_bs_saw_iter[$index]\n";
            print OUT_REALLOC " MEW_ITER $min_bs_mew_iter[$index]\n";
            print OUT_REALLOC " TOPSIS_ITER $min_bs_topsis_iter[$index]\n";
            print OUT_REALLOC " GAP $min_bs_gap[$index]\n";
            print OUT_REALLOC " GA $min_bs_mo[$index]\n";

            $hash_old_saw{$gene}{$app_ass} = $min_bs_saw[$index];
            $hash_old_mew{$gene}{$app_ass} = $min_bs_mew[$index];
            $hash_old_topsis{$gene}{$app_ass} = $min_bs_topsis[$index];
            $hash_old_saw_iter{$gene}{$app_ass} = $min_bs_saw_iter[$index];
            $hash_old_mew_iter{$gene}{$app_ass} = $min_bs_mew_iter[$index];
            $hash_old_topsis_iter{$gene}{$app_ass} = $min_bs_topsis_iter[$index];
            $hash_old_gap{$gene}{$app_ass} = $min_bs_gap[$index];
            $hash_old_ga{$gene}{$app_ass} = $min_bs_mo[$index];
            $index ++;
        }

        print "OLD BS   @app_assign_id  SAW @min_bs_saw MEW @min_bs_mew GA @min_bs_mo\n"; print OUT_REALLOC "OLD BS @app_assign_id  SAW @min_bs_saw MEW @min_bs_mew GA @min_bs_mo\n";
    }

}


## Reallocs ALL


################# ENERGY RANKING INTERFACE SELECTION ###############
sub energy_opt2()
{}

sub energy_opt()
{

    print "***** ENERGY OPT\n";
    # lire les fichiers APPS et INTERFACES
    my %apps = ();
    my %ifaces = ();
    my $time_min = 10000000;
    my $pow_min = 10000000;
    my $last_id = 0;
    
    my $app_count = 0;
    my $count_iface = 0;

    my %hash_min = ();
    my @min_iface_array=();

    open AP, "../apps";
    open IF, "../interfaces";

    my $id = 0;

    foreach my $line (<AP>)
    {
        #print "AP\n";
        my @line = split(' ',$line);

        print @line;

        if ($line[0] eq 'app')
        {
            $id = $app_count;
            $last_id = $app_count;
            print "ID $id\n";
            $app_count++; 
                        
        }elsif ($line[0] eq 'size')
        {
            $apps{$id}{'size'}=$line[1]; print "$apps{$id}{'size'}\n";

        }elsif ($line[0] eq 'bdw')
        {
            $apps{$id}{'bdw'}=$line[1]; print "$apps{$id}{'bdw'}\n";

        }elsif ($line[0] eq 'time')
        {
            $apps{$id}{'time'}=$line[1]; print "$apps{$id}{'time'}\n";
            
            if ($line[1]<$time_min)
            {
                $time_min = $line[1];
                print "TIME MIN: $time_min\n";
            }
        }
    }

    foreach my $line (<IF>)
    {
        #print "IF\n";
        my @line = split(' ',$line);

        chomp($line[1]);

        if ($line[0] eq 'iface')
        {
            $id = $line[1]; print "ID $id\n";
            $count_iface++;
            
        }elsif ($line[0] eq 'type')
        {
            $ifaces{$id}{'type'}=$line[1]; print "$ifaces{$id}{'type'}\n";

        }elsif ($line[0] eq 'av_bdw')
        {
            $ifaces{$id}{'av_bdw'}=$line[1]; print "$ifaces{$id}{'av_bdw'}\n";

        }elsif ($line[0] eq 'pow1')
        {
            $ifaces{$id}{'pow1'}=$line[1]; print "$ifaces{$id}{'pow1'}\n";

            $hash_min{$id} = $line[1];

        }elsif ($line[0] eq 'pow2')
        {
            $ifaces{$id}{'pow2'}=$line[1]; print "$ifaces{$id}{'pow2'}\n";

        }elsif ($line[0] eq 'pow3')
        {
            $ifaces{$id}{'pow3'}=$line[1]; print "$ifaces{$id}{'pow3'}\n";

        }elsif ($line[0] eq 'timer1')
        {
            $ifaces{$id}{'timer1'}=$line[1]; print "$ifaces{$id}{'timer1'}\n";

        }elsif ($line[0] eq 'timer2')
        {
            $ifaces{$id}{'timer2'}=$line[1]; print "$ifaces{$id}{'timer2'}\n";
        }
    }

    close (AP);
    close (IF);

    foreach my $key (sort {$hash_min{$a}<=>$hash_min{$b}} keys %hash_min )
    {
        push(@min_iface_array, $key);
        print "IFACE $key\n";
    }
    # Ordennancer les interfaces par CONSOMMATION en emission/réception
    # à l'arrivé de chaque application l'assigner à chaque interface

    my $bdw = 0;
    my $total_bdw = 0;
    my $bdw_obj = 0;
    my $energy = 0;
    my $iface_index = 0;
    my $TB;
    my $TI;
    my @solution = ();
    
    #print "READ APPS\n";

    foreach my $id (sort {$a<=>$b} keys %apps)
    {
        print "READ APPS\n";

        print "OBJS BD:$bdw_obj EN:$energy\n";

        $bdw = $apps{$id}{'bdw'};
        $total_bdw += $bdw;
        print "****** APP $id   BDW: $bdw\n";
        print "****** IFA $min_iface_array[$iface_index]    BDW:$ifaces{$min_iface_array[$iface_index]}{'av_bdw'}   DEMAND: $total_bdw\n";

        push (@solution, $iface_index);

        if ((($total_bdw >= $ifaces{$min_iface_array[$iface_index]}{'av_bdw'}) && ($iface_index != ($count_iface - 1) ) ) || ((($total_bdw >= $ifaces{$min_iface_array[$iface_index]}{'av_bdw'}) && ($iface_index == ($count_iface - 1) ) && ($id == $last_id))  ) )  #interface saturé
        {
            
            print "Interface Saturé\n";
            $energy += $ifaces{$min_iface_array[$iface_index]}{'pow1'} * $time_min; 

            $bdw_obj += ($total_bdw - $ifaces{$min_iface_array[$iface_index]}{'av_bdw'});           

            $total_bdw = 0;
            $iface_index ++; #go to the next interface in the sequence

        } elsif (($id == $last_id) && ($total_bdw < $ifaces{$min_iface_array[$iface_index]}{'av_bdw'} ))
        {
            #only energy for the last interface since not bandwidth insatisfaction
            print "Last application\n";
            my $ratio = $total_bdw / $ifaces{$min_iface_array[$iface_index]}{'av_bdw'};

            if ($ratio <= 1)
            {
                $TB = $ratio * $time_min;
                $TI = $time_min - $TB;
            } else
            {
                $TB = $time_min;
                $TI = 0;
            }   

            print "TB: $TB  TI: $TI\n";

            if ($ifaces{$min_iface_array[$iface_index]}{'type'} == 0) #3G
            {
                if ($TI < $ifaces{$min_iface_array[$iface_index]}{'timer1'})
                {
                    print "3G 1\n";
                    $energy += ($TB+$TI) * $ifaces{$min_iface_array[$iface_index]}{'pow1'};

                } elsif ($TI < ($ifaces{$min_iface_array[$iface_index]}{'timer1'}+$ifaces{$min_iface_array[$iface_index]}{'timer2'}))
                {
                    print "3G 2\n";
                    $energy += ($TB+$ifaces{$min_iface_array[$iface_index]}{'timer1'}) * $ifaces{$min_iface_array[$iface_index]}{'pow1'} + ($TI - $ifaces{$min_iface_array[$iface_index]}{'timer1'}) * $ifaces{$min_iface_array[$iface_index]}{'pow2'};
                
                } else
                {
                    print "3G 3\n";
                    $energy += ($TB+$ifaces{$min_iface_array[$iface_index]}{'timer1'}) * $ifaces{$min_iface_array[$iface_index]}{'pow1'} + ($ifaces{$min_iface_array[$iface_index]}{'timer2'}) * $ifaces{$min_iface_array[$iface_index]}{'pow2'} + ($TI-$ifaces{$min_iface_array[$iface_index]}{'timer1'}-$ifaces{$min_iface_array[$iface_index]}{'timer2'}) * $ifaces{$min_iface_array[$iface_index]}{'pow3'};
                }
            }
             else #WLAN
            {
                if ($TI < $ifaces{$min_iface_array[$iface_index]}{'timer1'})
                {
                    print "WLAN 1\n";
                    $energy +=  $TB*$ifaces{$min_iface_array[$iface_index]}{'pow1'} + $TI * $ifaces{$min_iface_array[$iface_index]}{'pow2'};
                } else
                {
                    print "WLAN 2\n";
                    $energy += $TB*$ifaces{$min_iface_array[$iface_index]}{'pow1'} + $ifaces{$min_iface_array[$iface_index]}{'timer1'} * $ifaces{$min_iface_array[$iface_index]}{'pow2'} + ($TI-$ifaces{$min_iface_array[$iface_index]}{'timer1'}) * $ifaces{$min_iface_array[$iface_index]}{'pow3'};
                }

            }       
        }       

    }
    # ---> Deux manières possibles: 1) Jusqu'à la prémière que dépase la BP disponible 2) Jusqu'à celle avant

    #

    my $pow = $energy / (1000*$time_min);
    print "ENERGY: $pow BAND: $bdw_obj\n";
    print OUTPUT_OBJ "$pow  $bdw_obj\n";

    open OUTPUT_SOL,">../output_sol\n";

    foreach my $item (<@solution>)
    {
        print OUTPUT_SOL "$item ";
    }

    close(OUTPUT_SOL);   


    $energy_er = $pow;
    $bdw_er = $bdw_obj;
}

################# MAIN #################


#FLOWS SIMULATION

my $time_day = time();
my $mkdir = `mkdir $time_day`;

print "\n#### FLOW INIT\n";

my $max_time;

my $nb_arrivals = 20;
#my $nb_arrivals = int(&rand_geom(1/5)) + 1; # number or flow arrivals
print "FLOWS = $nb_arrivals\n";

my @flow_arrival = &poisson($nb_arrivals, 1/30); # flow arrival process 1/60
print "ARRIVAL = @flow_arrival\n";

open WIN,">result_win";
open ARRIVAL, ">../arrival";

foreach my $item (@flow_arrival)
{
    print ARRIVAL "$item    0.001\n";
} 


open OUTPUT, ">../output";
open FLOWGRAPH, ">../flowgraph";
open BDWGRAPH, ">../bdwgraph";
open REALLOCS, ">../reallocs";
open REALLOCS_ALL, ">../reallocs_all";

open FLOW_FOR_ENER_CALC, ">../flow_for_ener_calc";
open IFACES_FOR_ENER_CALC, ">../ifaces_for_ener_calc";
#open IFACE_FLOW_SELECTION, ">../iface_flow_selection";


# for each flow arrival, we simulate a traffic session

my $count = 0;
my %type_flow = ();

foreach my $flow (@flow_arrival)
{
    if (rand()>0.4)
    {   print "HTTP Flow\n";
        print OUTPUT "HTTP Flow\n";     
        &gen_http($count, $flow);

        $type_flow{$count} = 0;

        $count ++;
    } else 
    {   print "RT Flow\n";
        print OUTPUT "RT Flow\n";
        &gen_rt($count, int(rand()*3), $flow);

        $type_flow{$count} = 1;

        $count ++;
    }
}

#print flows

my $sum_data = 0;

my $old_time = 0;
my $dif = 0;
my $old_data = 0;
my $inst_bwd = 0;

my $first_flag = 0;

my %band_hash = ();

foreach my $id (sort {$a<=>$b} keys %flows)
{
    print "FLOW $id:\n";
    print OUTPUT "FLOW $id:\n";

    foreach my $time ( sort {$a<=>$b} keys %{$flows{$id}} )
    {
        $band_hash{$time} = $flows{$id}{$time}; 
        
        $sum_data += $flows{$id}{$time};
        print FLOWGRAPH "$time  $flows{$id}{$time}  $type_flow{$id} $id\n";
        print " T=$time S=$flows{$id}{$time}\n";
        print OUTPUT "  T=$time S=$flows{$id}{$time}\n";
		print FLOW_FOR_ENER_CALC "FLOW\t$id\tTIME\t$time\tSIZE\t$flows{$id}{$time}\n"; #For energy calculation 
        if ($time > $max_time)
        {
            $max_time = $time;
        }
    }

    print "\n";
}

my $old_data = 0;
my $old_time = 0;
my $bdw = 0;

foreach my $time (sort {$a<=>$b} keys %band_hash)
{
    if ($time != 0)
    {
        $bdw = $old_data / ($time - $old_time);
        print BDWGRAPH "$old_time   $bdw\n";        
    }

    $old_time = $time;
    $old_data = $band_hash{$time};
}

print "SIMULATION ENDS AT: $max_time\n"; 
print OUTPUT "SIMULATION ENDS AT: $max_time\n"; 

my $band = $sum_data / $max_time;
print "BANDWIDTH: $band\n"; 
print OUTPUT "BANDWIDTH: $band\n"; 

#INTERFACES SIMULATION

print "\n#### INTERFACE INIT\n";
print OUTPUT "\n#### INTERFACE INIT\n";

$nb_ifaces = 4; # 3 interfaces by default

for (my $i=0; $i< $nb_ifaces; $i++)
{
    my $status_init = int(rand()*4);
    print "STATUS INIT IFACE $i: $status_init \n";
    print OUTPUT "STATUS INIT IFACE $i: $status_init \n";
	print IFACES_FOR_ENER_CALC "TIME\t0\tIFACE\t$i\tSIZE_STATUS\t$status_init\n"; #For energy calculation
    
    my $temps_con;
    my $temps_desc;
    $temps_con = 0;
    $temps_desc = 0;
    
    my $lambda_con;
    my $lambda_desc;
    my $min_bw;
    my $max_bw;
    
    if ($i == 0) #3G
    {
        $ifaces_type{$i} = 0;
        $lambda_con = 1/1800;
        #$lambda_con = 1/180000000000;
        $lambda_desc = 1/2;
        $max_bw = 125000;   #bytes/s    
        $min_bw = 1000; #bytes/s

    } else #WiFi
    {
        $ifaces_type{$i} = 1;
        $lambda_con = 1/300;
        #$lambda_con = 1/180000000000;      
        $lambda_desc = 1/10;
        $max_bw = 250000; #bytes/s
        $min_bw = 10000;
    } 

    #temps desc/conn et bw

    while (($temps_con < $max_time) && ($temps_desc < $max_time))
    {
        #print "TEMPS CON: $temps_con   TEMPS_DIS: $temps_desc  MAX: $max_time\n";
        if ($status_init == 0) #IFACE OFF AT INIT
        {
        
            $temps_desc += $temps_con + &rand_exp($lambda_desc);

            if ($temps_desc <= $max_time)
            {
                $events{$temps_desc} = "IFACE_ON";
                $events_id{$temps_desc} = $i;
            }

            my $delta = &rand_exp($lambda_con);
            $temps_con += $temps_desc + $delta;

            if ($temps_con <= $max_time)
            {
                $events{$temps_con} = "IFACE_OFF";
                $events_id{$temps_con} = $i;
            }

            my $band = int(rand()*$max_bw) + $min_bw;
        
            $ifaces{$i}{$temps_con} = $band;
            $ifaces{$i}{$temps_desc} = 0;

        } else #IFACE ON AT INIT
        {
            my $delta = &rand_exp($lambda_con);
            $temps_con += $temps_desc + $delta;

            if ($temps_con <= $max_time)
            {
                $events{$temps_con} = "IFACE_OFF";
                $events_id{$temps_con} = $i;
            }

            my $band = int(rand()*$max_bw) + $min_bw;
        
            $temps_desc += $temps_con + &rand_exp($lambda_desc);

            if ($temps_desc <= $max_time)
            {
                $events{$temps_desc} = "IFACE_ON";
                $events_id{$temps_desc} = $i;
            }

            $ifaces{$i}{$temps_con} = $band;
            $ifaces{$i}{$temps_desc} = 0;
        }
    }

        #Energy parameters

        $ifaces_energy{$i}{1} = int(rand()*$max_pow1) + $min_pow1;
        $ifaces_energy{$i}{2} = int(rand()*$max_pow2) + $min_pow2;
        $ifaces_energy{$i}{3} = int(rand()*$max_pow3) + $min_pow3;

        $ifaces_timers{$i}{1} = int(rand()*$max_timer1) + $min_timer1;
        $ifaces_timers{$i}{2} = int(rand()*$max_timer2) + $min_timer2;

}

foreach my $id (sort {$a<=>$b} keys %ifaces)
{
    print "IFACE $id:\n";
    print OUTPUT "IFACE $id:\n";
    
    open IFACEGRAPH, ">../ifacegraph_$id";
    print IFACEGRAPH "0 0\n";

    foreach my $time ( sort {$a<=>$b} keys %{$ifaces{$id}} )
    {
        print " T=$time STATUS=$ifaces{$id}{$time}\n";
        print OUTPUT "  T=$time STATUS=$ifaces{$id}{$time}\n";
        print IFACEGRAPH "$time $ifaces{$id}{$time}\n";
		print IFACES_FOR_ENER_CALC "IFACE\t$id\tTIME\t$time\tSIZE_STATUS\t$ifaces{$id}{$time}\n"; # For energy calculation
    }

    close IFACEGRAPH;

    print "\n";
}

$count = keys %events;

print "NUMBER OF EVENTS: $count\n\n";
print OUTPUT "NUMBER OF EVENTS: $count\n\n";

my $apps_count = 0;
my $ints_count = 0;

foreach my $event (sort {$a<=>$b} keys %events)
{
    
    $apps_count = 0;
    $ints_count = 0;
    @app_assign_id = ();

    #my $win_end = $event + $winSize;

    open APPS, ">../apps";
    open IFACES, ">../interfaces";

    open OUTPUT_OBJ, ">../output_obj_energy";

    my $id_new = 0;
    my $delta = 0;
    my $nb_bursts = 3;

    # We consider only NEW FLOWS as EVENTS
    if ($events{$event} eq 'NEW_FLOW')
    {
        
        print "############################\n";
        print OUTPUT "############################\n";
        print "EVENT Time: $event   Type: $events{$event}\n";
        print OUTPUT "EVENT Time: $event    Type: $events{$event}\n";
    
        my $id = $events_id{$event};
        $id_new = $id;

        print "FLOW $id:\n";
        print OUTPUT "FLOW $id:\n";

        #count the time to the $nb_bursts 

        my $i = 0;
        my $data = 0;
        my $bdw = 0;
        my $bursts = 1;

        foreach my $time (sort {$a<=>$b} keys %{$flows{$id}} )  
        {
            $i++;


            if ( ($i <= $nb_bursts)&&($i < (keys %{$flows{$id}}) ))
            {
                $data += $flows{$id}{$time};
                $delta = $time - $event;
                print "DELTA: $delta    DATA: $data\n";     
                print OUTPUT "DELTA: $delta DATA: $data\n";     
            } else {
                $delta = $time - $event;
                $bdw = $data / $delta;
                $bursts = 0;
                print "DELTA: $delta    DATA: $data\n"; 
                print OUTPUT "DELTA: $delta DATA: $data\n";                                         
                last;
            }   


        }

        $apps_count ++;
        
		if ($apps_count < 13) #code added for limiting amount of apps
		{
			push (@app_assign_id, $id);
			print APPS "app $id\n";
			print APPS "size $data\n";
			print APPS "bdw $bdw\n";
			print APPS "time $delta\n";
			#print APPS "bursts $bursts\n";
				
			print "app $id\n";
			print "size $data\n";
			print "bdw $bdw\n";
			print "time $delta\n";
			print OUTPUT "app $id\n";
			print OUTPUT "size $data\n";
			print OUTPUT "bdw $bdw\n";
			print OUTPUT "time $delta\n";
			#print "bursts $bursts\n";
		} else {
			$apps_count --;
		}
        # using the $delta time see other active flows
        foreach my $id (sort {$a<=>$b} keys %flows)
        {
            print "ID = $id\n";
            print OUTPUT "ID = $id\n";
            $data = 0;
            $bdw = 0;
            my @time_all = ();
            my @data_all = ();
            my $time_old = 0;
            $bursts = 1;
            my $multipleApps = 0;
            my $first = 0;

            if ($id != $id_new)
            {       
                $first = 1;
                #print "Good ID $id!!\n";
                print OUTPUT "Good ID $id!!\n";     
                foreach my $time ( sort {$a<=>$b} keys %{$flows{$id}} )
                {           
                    
                    if (($event > $time_old)&&($event < $time)&&($first == 0))
                    {
                        #the flow is in the middle of the event (new flow)
                        print "EVENT $event > TIME_OLD $time_old    EVENT $event < TIME $time\n";
                        print OUTPUT "EVENT $event > TIME_OLD $time_old EVENT $event < TIME $time\n";
                        $multipleApps = 1;
                        $time_old = $time;

                        last;
                    } else {
                        #print "PUSH $time\n";
                        push (@time_all, $time);
                        push (@data_all, $flows{$id}{$time});                   
                    }

                    #if (($time <= $event) && ($time >= ($event-$delta)))
                    #{
                    #   $data += $flows{$id}{$time};
                    #   print "DELTA: $delta    -   DATA: $data\n";
                    #} 
        
                    #if ($time > $event)
                    #{  
                    #   print "$time > $event\n";
                    #   last;
                    #}

                    $time_old = $time;
                    $first = 0;
    
                }

                #at this point we should take the last elements of the array
                my $size = scalar (@data_all);
                my $total_time = 0;
                my $total_data = 0;


                if ($multipleApps == 1) 
                {
                    print "SIZE $size   BURSTS $nb_bursts\n";
                    print OUTPUT "SIZE $size    BURSTS $nb_bursts\n";
                    if ($size < $nb_bursts)
                    {
                        print "SIZE $size < BURSTS $nb_bursts\n";
                        print OUTPUT "SIZE $size < BURSTS $nb_bursts\n";
                        $nb_bursts = $size;
                        $bursts = 0;
                    }

                    for (my $i = $size - 1; $i > ($size - $nb_bursts -1); $i--)
                    {
                        $total_time += ($time_old - $time_all[$i]);
                        $total_data += $data_all[$i]; 

                        $time_old = $time_all[$i];
                        
                        print "$i   DATA $total_data    TIME $total_time\n";
                        print OUTPUT "$i    DATA $total_data    TIME $total_time\n";
                                            
                    }

                    if ($total_data > 0)
                    {
                        print "DATA $total_data > 0\n";
                        print OUTPUT "DATA $total_data > 0\n";

                        my $bdw = $total_data / $total_time;

                        $apps_count ++;
                        
						if ($apps_count < 13) #code added for limiting amount of apps
						{
							push (@app_assign_id, $id);
							print APPS "app $id\n";
							print APPS "size $total_data\n";
							print APPS "bdw $bdw\n";
							print APPS "time $total_time\n";
							#print APPS "bursts $bursts\n";
							
							print "app $id\n";
							print "size $total_data\n";
							print "bdw $bdw\n";
							print "time $total_time\n";
							print "bursts $bursts\n";
							print OUTPUT "app $id\n";
							print OUTPUT "size $total_data\n";
							print OUTPUT "bdw $bdw\n";
							print OUTPUT "time $total_time\n";
							#print OUTPUT "bursts $bursts\n";
						} else {
							$apps_count --;
						}
                    }
                }
            }       
        }
    

    #APSS
    #foreach my $id (sort {$a<=>$b} keys %flows)
    #{
    #   print "FLOW $id:\n";
    #
    #   my $data = 0;
    #
    #   foreach my $time ( sort {$a<=>$b} keys %{$flows{$id}} )
    #   {           
    #       
    #       if (($time >= $event) && ($time <= $win_end))
    #       {
    #           print "TIME = $time - EVENT: $event - END: $win_end\n";
    #           $data += $flows{$id}{$time};
    #       } 
        #
    #       if ($time > $win_end)
    #       {
    #           last;
    #       }
    #   }

    #   if ($data > 0)
    #   {
    #       print APPS "app $id\n";
    #       print APPS "size $data\n";
    #       
    #       print "app $id\n";
    #       print "size $data\n";
        #
    #   }
    #}

    close APPS;


    #IFACE
    foreach my $id (sort {$a<=>$b} keys %ifaces)
    {
        print "IFACE $id:\n";
        print OUTPUT "IFACE $id:\n";
    
        my $time_old = 0;
        foreach my $time ( sort {$a<=>$b} keys %{$ifaces{$id}} )
        {
            print " T=$time STATUS=$ifaces{$id}{$time}\n";
            print OUTPUT "  T=$time STATUS=$ifaces{$id}{$time}\n";
            
            if ((($event >= $time_old) && ($event<$time)) || ($event == 0)) 
            {
                print "iface $id\n";
                print "av_band $ifaces{$id}{$time}\n";
                print OUTPUT "iface $id\n";
                print OUTPUT "av_band $ifaces{$id}{$time}\n";

                if ($ifaces{$id}{$time} > 0)
                {

                    $ints_count ++;
                    print IFACES "iface $id\n";
                    print IFACES "type $ifaces_type{$id}\n";
                    print IFACES "av_bdw $ifaces{$id}{$time}\n";
                    print IFACES "pow1 $ifaces_energy{$id}{1}\n";
                    print IFACES "pow2 $ifaces_energy{$id}{2}\n";
                    print IFACES "pow3 $ifaces_energy{$id}{3}\n";
                    print IFACES "timer1 $ifaces_timers{$id}{1}\n";
                    print IFACES "timer2 $ifaces_timers{$id}{2}\n";
                }       
                last;
            } 

            $time_old = $time;
        }
    }

    close (IFACES);

    

    # at this point both apps and ifaces files are fullfiled ...
    # 1) We may trigger the decision algorithm (PISA)
    # 2) We may calculate other assignation algorithms
    #
    #For testing amount of ifaces and applications
    #if ( $apps_count == 8 ) {
    #
    ###
    ###
    my $flag_all = 0;
    
    foreach my $gen (@generations)
    {

        @array_bs_saw = ();
        @array_bs_mew = ();
        @array_bs_ga = ();

        if ($gen == 10)
        {
            $update_realloc = 1;
            $update_realloc_all = 1;
        } else
        {
            #$update_realloc = 0;
            $update_realloc = 1;
            $update_realloc_all = 1;
        }

        if ($ints_count > 1)
        {   
            #Global file for calculation
            open GLOBAL_DELAY, ">>global_delay_$gen.txt";
            #MO file
            open MO_PARAM, ">../ifacesel11_param.txt";
            open DISTANCE, ">>distance";
            open NUMBER_SOL, ">>number_sol";
            open DISTANCE_SOL, ">>distance_sol";
            open COUNTS_SOL, ">>counts_sol";
            open DIFF_MADM_MO, ">>diff_madm_mo";
            open DIFF_TOPSIS_MO, ">>diff_topsis_mo";
            open RADIO_DIST_MIN, ">>radio_dist_min";

            #Global file for weight sensibility calcultion
            
            #open GLOBAL_WEIGHTS_NB_SOLS, ">>global_weights_nb_sols.txt";
            #open GLOBAL_WEIGHTS_NB_SOLS_IMPROVED, ">>global_weights_nb_sols_improved.txt";
            #open GLOBAL_MADM_WEIGHT_DIST_SAW, ">>global_madm_weigth_dist_saw.txt";
            #open GLOBAL_MADM_WEIGHT_DIST_MEW, ">>global_madm_weigth_dist_mew.txt";
            #open GLOBAL_MADM_WEIGHT_DIST_TOPSIS, ">>global_madm_weigth_dist_topsis.txt";
            
            
            
            my $seed = int(rand()*1000);

            print MO_PARAM "seed $seed\n";
            print MO_PARAM "maxgen $gen\n";
            print MO_PARAM "outputfile output.txt\n";
            print MO_PARAM "mutation_type 1\n";
            print MO_PARAM "recombination_type 2\n";
            print MO_PARAM "mutation_probability 1\n";
            print MO_PARAM "recombination_probability 1\n";
            print MO_PARAM "bit_turn_probability 0.9\n";

            close MO_PARAM;

            #open(SEMAPHORE, ">semaphore");
            #print SEMAPHORE "0";
            #print "SEMAPHORE 0\n";
            #close(SEMAPHORE);

            #my $pid = fork();

            #MULTI-OBJECTIVE SOLUTION - Run selector and variator as two different processes
            #if (not defined $pid){
                #print "Fork Not Possible\n";
                #print OUTPUT "Fork Not Possible\n";


            #} elsif ($pid == 0) { #Variator
                print "Execute Variator\n";
                print OUTPUT "Execute Variator\n";      
                my $result1 = `python ../../new_nsga2/examples/main.py ../ifacesel11_param.txt ../interfaces ../apps ../../PISA_ 0.0001 $flag_all $event`;
                #print "python ../../new_nsga2/examples/main.py ../ifacesel11_param.txt ../interfaces ../apps ../../PISA_ 0.0001 $flag_all $event";
                #print $result1;
                print "RESULT VAR $result1\n";
                open(SEMAPHORE, ">semaphore");
                print SEMAPHORE "1";
                print "SEMAPHORE 1\n";
                close(SEMAPHORE);
                print "Variator Executed\n";
                print OUTPUT "Variator Executed\n";
                #exit(0);           

            #} else { #Selector
    
                #sleep 1;
                #print "Execute Selector\n";    
                #print OUTPUT "Execute Selector\n";                         
                #my $result2 = `./../../NSGA2/nsga2 ../../NSGA2/nsga2_param.txt ../../PISA_ 0.0001`;
                #my $result2 = `./../../spea2/spea2 ../../spea2/spea2_param.txt ../../PISA_ 0.0001`;
                #print "RESULT SEL $result2\n";
                #print "Selector Executed\n";
                #print OUTPUT "Selector Executed\n";

            #}

            #open(SEMAPHORE, "semaphore");
            #my $semaphore = <SEMAPHORE>;
            #close(SEMAPHORE);

            #print "$semaphore\n";

            #while ($semaphore == 0)
            #{
                #open(SEMAPHORE, "semaphore");
                #$semaphore = <SEMAPHORE>;
                #close(SEMAPHORE);
                #print "$semaphore\n";
            #}

            #print "$semaphore\n"; 

            print "**** CALL ENERGY OPT\n";
            &energy_opt();
            close(OUTPUT_OBJ);

            # See if we perform better than the Energy Ranking

            open OUTPUT_MO, "output.txt";


            my $win = 0; # combien de fois on fait mieux que ER
            my $loose = 0;
            my $ind = 0;

            my @energy_dist = ();
            my @band_dist = ();

            foreach my $line (<OUTPUT_MO>)
            {
                my @line_split = split ('\t',$line);
    
                my $energy_mo = $line_split[2];
                my $bdw_mo = $line_split[4];

                print "SOL  $energy_mo  $bdw_mo\n";
                push (@energy_dist, $energy_mo);
                push (@band_dist, $bdw_mo);

                if ( ($energy_mo <= $energy_er) && (int($bdw_mo) <= int($bdw_er)) )
                {
                    $win ++;
                    last;
                } elsif ( ($energy_mo > $energy_er) && (int($bdw_mo) > int($bdw_er)) )
                {
                    $loose++;
                }
            }

        
            if ($win > 0)
            {
                $win_tot ++;
                print WIN "$event   WIN\n";

            } elsif ($loose > 0)
            {
                $loose_tot ++;
                print WIN "$event   LOOSE\n";
            }

            close (OUTPUT_MO);


        
            open TIME, "time";

            my $flag = 0;
            my $index = 0;
            my $nb_chromo = 0;

            while ($flag==0)
            {
                    if ((2**$index)<$ints_count)
                    {   $index++;
                }           
                else
                    {   $flag = 1;
                } 
            }

            $nb_chromo = $index * $apps_count;

            foreach my $line_delay (<TIME>)
            {
                my @array_delay = split ('\t', $line_delay);

                print"CHROMO $nb_chromo $array_delay[0] $array_delay[1] $array_delay[2] $array_delay[3]\n";

                if ($gen == 10)
                {           
                    print GLOBAL_DELAY "$nb_chromo\t$array_delay[1]\t $array_delay[3]\t$array_delay[5]\t$array_delay[7]\t$array_delay[9]\n";
                    $time_all_global = $array_delay[3];
                    $time_madm_global = $array_delay[5];
                    $time_madm_2_global = $array_delay[7];
                    $time_gap_global = $array_delay[9];

                } else
                {
                    print GLOBAL_DELAY "$nb_chromo\t$array_delay[1]\t$time_all_global\t$time_madm_global\t$time_madm_2_global\t$time_gap_global\n";
                }

                $delay_calc{$nb_chromo}{$array_delay[0]} = $array_delay[1];
                $delay_calc{$nb_chromo}{$array_delay[2]} = $array_delay[3];

                $count_calc{$nb_chromo}{$array_delay[0]} ++;
                $count_calc{$nb_chromo}{$array_delay[2]} ++;
            }

            close (TIME);
            
            
            
            #open WEIGHTS_NB_SOL, "madm_weigth_nb_sols";
            #open WEIGHTS_NB_SOL_IMPROVED, "madm_weigth_nb_sols_improved";
            
             
            #open MADM_WEIGHT_DIST_SAW, "madm_weigth_dist_saw";
            #open MADM_WEIGHT_DIST_MEW, "madm_weigth_dist_mew";
            #open MADM_WEIGHT_DIST_TOPSIS, "madm_weigth_dist_topsis";

            #foreach my $line_nb_sols (<MADM_WEIGHT_DIST_SAW>)
            #{
                #my @array_weights_nb_sols = split ('\t', $line_nb_sols);

                #if ($gen == 10)
                #{  
                    #print GLOBAL_MADM_WEIGHT_DIST_SAW "$array_weights_nb_sols[1]\t $array_weights_nb_sols[3]\t$array_weights_nb_sols[5]\t$array_weights_nb_sols[7]\n";
                #} 
            #}
            #foreach my $line_nb_sols (<MADM_WEIGHT_DIST_MEW>)
            #{
                #my @array_weights_nb_sols = split ('\t', $line_nb_sols);

                #if ($gen == 10)
                #{           
                    #print GLOBAL_MADM_WEIGHT_DIST_MEW "$array_weights_nb_sols[1]\t $array_weights_nb_sols[3]\t$array_weights_nb_sols[5]\t$array_weights_nb_sols[7]\n";
                #} 
            #}
            #foreach my $line_nb_sols (<MADM_WEIGHT_DIST_TOPSIS>)
            #{
                #my @array_weights_nb_sols = split ('\t', $line_nb_sols);

                #if ($gen == 10)
                #{           
                    #print GLOBAL_MADM_WEIGHT_DIST_TOPSIS "$array_weights_nb_sols[1]\t $array_weights_nb_sols[3]\t$array_weights_nb_sols[5]\t$array_weights_nb_sols[7]\n";
                #} 
            #}

            #close (MADM_WEIGHT_DIST_SAW);
            #close (MADM_WEIGHT_DIST_MEW);
            #close (MADM_WEIGHT_DIST_TOPSIS);
            
            
            #foreach my $line_nb_sols (<WEIGHTS_NB_SOL>)
            #{
                #my @array_weights_nb_sols = split ('\t', $line_nb_sols);

                #if ($gen == 10)
                #{  
                    #print GLOBAL_WEIGHTS_NB_SOLS "$array_weights_nb_sols[1]\t $array_weights_nb_sols[3]\t$array_weights_nb_sols[5]\t$array_weights_nb_sols[7]\n";
                #} 
            #}
            
            #foreach my $line_nb_sols (<WEIGHTS_NB_SOL_IMPROVED>)
            #{
                #my @array_weights_nb_sols = split ('\t', $line_nb_sols);

                #if ($gen == 10)
                #{  
                    #print GLOBAL_WEIGHTS_NB_SOLS_IMPROVED "$array_weights_nb_sols[1]\t $array_weights_nb_sols[3]\t$array_weights_nb_sols[5]\t$array_weights_nb_sols[7]\n";
                #} 
            #}
            
            #close (WEIGHTS_NB_SOL);
            #close (WEIGHTS_NB_SOL_IMPROVED);
        

            #Compute goodness
            #We parse the set of ALL solutions and if there are solutions that outperforms MO solutiosn we calculate the euclidean distance to the closest MO solution
            #then we calculate, for all of these solutions, the average euclidean distance
            #High average euclidean distances mean that we are not covering well the objective space
            #distances close to ZERO shows a very good fit of the MO solutions

            #open MO_SOL, "output.txt";

            #my %ratios = ();
            #my $count_sol = 0;


            my $min_all_en = 999999999999;
            my $max_all_en = 0;
            my $min_all_bw = 999999999999;
            my $max_all_bw = 0;


            #@pareto_set = ();
            #foreach my $mo (<MO_SOL>)
            #{
                #my $min = 9999999999;
                #my $max = 0;
                #my $ratio = 0;

                #my @mo_sol = split('\t',$mo);

                #my $mo_bs = sprintf('%s', $mo_sol[5]);
                #my $mo_en = sprintf("%.2f", $mo_sol[2]);
                #my $mo_bw = sprintf("%.2f", $mo_sol[4]/1000);

                #print "mo_bs $mo_bs\n";

                my $all_bs = 0;

                #if (&not_in_set($mo_bs) == 0)
                #{
                    #$count_sol ++;
                    #push (@pareto_set, $mo_bs);
                    open ALL_SOL, "all_solutions";

                    #print "MO-EN $mo_en    MO_BW $mo_bw\n";
                    foreach my $all (<ALL_SOL>)
                    {
                        my @all_sol = split('\t',$all);

                        $all_bs = sprintf('%s', $all_sol[0]);
                        my $all_en = sprintf("%.2f", $all_sol[2]);
                        my $all_bw = sprintf("%.2f", $all_sol[4]/1000);

                        #print "    ALL-EN $all_en  ALL_BW $all_bw\n";

                        #if (($all_bw < $mo_bw + 0.1) && ($all_bw > $mo_bw - 0.1))
                        ##if ($all_bw == $mo_bw)            
                        #{
                        #   if ($all_en > $max)
                        #   {
                        #       $max = $all_en;
                        #   }
            
                        #   if ($all_en < $min)
                        #   {
                        #       $min = $all_en;
                        #   }
                        #}

                        if ($all_en < $min_all_en)
                        {
                            $min_all_en = $all_en;
                        } 

                        if ($all_bw < $min_all_bw)
                        {
                            $min_all_bw = $all_bw;
                        }

                        if ($all_en > $max_all_en)
                        {
                            $max_all_en = $all_en;
                        } 

                        if ($all_bw > $max_all_bw)
                        {
                            $max_all_bw = $all_bw;
                        }
                    }

                    close ALL_SOL;

                    #$ratio = ($mo_en - $min) / ($max - $min);
    
                    #if ($all_bs eq $mo_bs) 
                    #{
                    #   $ratio = 0;
                    #} else
                    #{      
                    #   $ratio = ($mo_en - $min) / ($mo_en);
            
                    #}

                    #if (exists $ratios{$mo_bw})
                    #{
                    #   if (($ratio < $ratios{$mo_bw}) && ($ratio >= 0) )
                    #   {
                    #       $ratios{$mo_bw} = $ratio;
                    #
                    #   } elsif ($ratio < 0)
                    #   {
                    #       $ratios{$mo_bw} = 0;
                    #   }

                    #}else
                    #{
                    #   if ($ratio >= 0)
                    #   {
                    #       $ratios{$mo_bw} = $ratio;
                    #   } else
                    #   {
                    #       $ratios{$mo_bw} = 0;
                    #   }
                    #}      

                    #print DISTANCE "BAND $mo_bw    RATIO $ratio    MIN $min    MAX $max    MO-EN $mo_en\n";
                #}

            #}

            #print NUMBER_SOL "$gen $count_sol\n";

            #foreach my $key (sort keys %ratios)
            #{
            #   print DISTANCE "$gen    $ratios{$key}\n";
            #}

            ##my $sum_distance = 0;
            ##my $count_distance = 0;

            #my $count_all = 0;
            #my $count_all_trade = 0;

            #foreach my $all (<ALL_SOL>)
            #{
                #my $prod = 0;
                ##my $distance = 0;
                ##my $min_distance = 99999999;

                #open MO_SOL, "output.txt";
                #my @all_sol = split('\t',$all);

                #my $all_en = sprintf("%.2f", $all_sol[2]);
                #my $all_bw = sprintf("%.2f", $all_sol[4]/1000);

                #foreach my $mo (<MO_SOL>)
                #{
                    #my @mo_sol = split('\t',$mo);

                    #my $mo_en = sprintf("%.2f", $mo_sol[2]);
                    #my $mo_bw = sprintf("%.2f", $mo_sol[4]/1000);

                    ##print "ALL $all_sol[2] $all_sol[4]    MO $mo_sol[2] $mo_sol[4]\n";
    
                    ##if ((($all_en <= $mo_en)&&($all_bw <= $mo_bw)) || 
                    ##    (($all_en == $mo_en)&&($all_bw <= $mo_bw)) ||
                    ##    (($all_en <= $mo_en)&&($all_bw == $mo_bw)) )
                    ##{
                        ## The solution is equal/better than MO
                    ##  $prod = 1;
                    ##  $distance = (($all_en - $mo_en)**2 + ($all_bw - $mo_bw)**2)**(0.5);
        
                    ##  if ($distance < $min_distance)
                    ##  {
                    ##      $min_distance = $distance;
                    ##      print DISTANCE "MIN DISTANCE    $min_distance   ALL $all_en $all_bw MO $mo_en $mo_bw\n";
                    ##  } 

                    ##} else
                    ##{
                    ##  $prod = 0;
                    ##  last;
                    ##}

                    ##if ($prod == 1) #the solution is equal/better
                    ##{
                    ##  $sum_distance += $distance;
                    ##  $count_distance ++;
                    ##}

                    #if ((($all_en < $mo_en)&&($all_bw < $mo_bw)) || 
                        #(($all_en == $mo_en)&&($all_bw < $mo_bw)) ||
                        #(($all_en < $mo_en)&&($all_bw == $mo_bw)) )
                    #{
                        #$prod = 1;

                    #} elsif ((($all_en < $mo_en)&&($all_bw > $mo_bw)) || 
                        #(($all_en > $mo_en)&&($all_bw < $mo_bw)))
                    #{

                        #$prod = 2;
                    #} else
                    #{
                        #$prod = 0;
                        #last;
                    #}

                #}

                #if ($prod == 1)
                #{
                    #$count_all ++;

                #} elsif ($prod ==2)
                #{  
                    #$count_all_trade ++;
                #} else
                #{
                    #last;
                #}

                #close MO_SOL;
            #}



        ##my $avg_dist = $sum_distance / $count_distance;
        #print DISTANCE "BEST   $count_all\n";
        #print DISTANCE "TRADE  $count_all_trade\n";
        #close DISTANCE;

            open OUTPUT_MO, "output.txt";
            open MADM, "madm";
            open MADM_MEW, "madm_mew";   
            open MADM_TOPSIS, "madm_topsis";
            open MADM_SAW_ITER, "madm_saw_iter";
            open MADM_MEW_ITER, "madm_mew_iter";   
            open MADM_TOPSIS_ITER, "madm_topsis_iter";
            open GAP, "gap";
            

            my $sum_dist_to_min = 0;
            my $count_dist_to_min = 0;

            my $count_mo = 0;
            my $count_madm = 0;
            my $count_mew;
            my $count_topsis;
            my $count_saw_iter;
            my $count_mew_iter;
            my $count_topsis_iter;
            my $count_gap;

            my $min_dist_to_min_mo = 99999999;
            my $max_dist_to_min_mo = 0;

            @pareto_set = ();
            foreach my $line (<OUTPUT_MO>)
            {

                my @line_split = split ('\t',$line);
    
                my $energy_mo = sprintf("%.2f", $line_split[2]);
                my $bdw_mo = sprintf("%.2f", ($line_split[4]/1000));
                $mo_bs = sprintf('%s', $line_split[6]);
            

                if (&not_in_set($mo_bs) == 0)
                {
                    $count_dist_to_min ++;
                    $count_mo++;

                    push (@pareto_set, $mo_bs);
                    push (@array_bs_ga, $mo_bs);

                    my $energy_mo_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_mo_norm = ($energy_mo-$min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_mo_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_mo_norm = ($bdw_mo - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    #my $dist_to_min = (($energy_mo - $min_all_en)**2 + ($bdw_mo - $min_all_bw)**2)**(0.5);
                    my $dist_to_min = (($energy_mo_norm)**2 + ($bdw_mo_norm**2))**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_mo)
                    {
                        $min_dist_to_min_mo = $dist_to_min;
                        @min_bs_mo = split(undef,$mo_bs);
                    }

                    if ($dist_to_min > $max_dist_to_min_mo)
                    {
                        $max_dist_to_min_mo = $dist_to_min;
                    }

                    #print RADIO_DIST_MIN " $energy_mo  $bdw_mo $energy_mo_norm $bdw_mo_norm    $min_all_en $min_all_bw $max_all_en $max_all_bw $dist_to_min\n";

                    #print "SOL $energy_mo_norm $bdw_mo_norm\n";
                    push (@energy_dist, $energy_mo_norm);
                    push (@band_dist, $bdw_mo_norm);
                }
            }

            #count number of MADM different solutions       
            @pareto_set = ();
        
            my $min_dist_to_min_madm = 99999999;
            my $max_dist_to_min_madm = 0;


            close (OUTPUT_MO);

            foreach my $line (<MADM>)
            {
                my @line_split = split ('\t',$line);
    
                $madm_bs = sprintf('%s', $line_split[10]);
                my $madm_bw = sprintf('%s', $line_split[4]);
                my $madm_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $madm_bw, $madm_en;

                my $madm_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $madm_en = sprintf('%.2f', $line_split[2]);

                print "MADM $madm_bs    $madm_bw    $madm_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_saw, $madm_bs);

                    $count_madm++;

                    my $energy_madm_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_madm_norm = ($madm_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_madm_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_madm_norm = ($madm_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_madm_norm)**2 + ($bdw_madm_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_madm)
                    {
                        $min_dist_to_min_madm = $dist_to_min;
                        
                        @min_bs_saw = split(undef,$madm_bs);
                        print "########################MIN SAW\n";
                        
                    }

                    if ($dist_to_min > $max_dist_to_min_madm)
                    {
                        $max_dist_to_min_madm = $dist_to_min;
                    }
                }
            }

            close (MADM);

            #count number of MEW different solutions        
            @pareto_set = ();
        
            my $min_dist_to_min_mew = 99999999;
            my $max_dist_to_min_mew = 0;


            foreach my $line (<MADM_MEW>)
            {

                my @line_split = split ('\t',$line);
    
                $mew_bs = sprintf('%s', $line_split[10]);
                my $mew_bw = sprintf('%s', $line_split[4]);
                my $mew_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $mew_bw, $mew_en;

                my $mew_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $mew_en = sprintf('%.2f', $line_split[2]);

                print "MEW  $mew_bs $mew_bw $mew_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_mew, $mew_bs);

                    $count_mew++;

                    my $energy_mew_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_mew_norm = ($mew_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_mew_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_mew_norm = ($mew_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_mew_norm)**2 + ($bdw_mew_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_mew)
                    {
                        $min_dist_to_min_mew = $dist_to_min;

                        @min_bs_mew = split(undef,$mew_bs);
                        print "########################MIN MEW\n";
                    }

                    if ($dist_to_min > $max_dist_to_min_mew)
                    {
                        $max_dist_to_min_mew = $dist_to_min;
                    }
                }
            }

            close (MADM_MEW);


            #count number of topsis different solutions       
            @pareto_set = ();
        
            my $min_dist_to_min_topsis = 99999999;
            my $max_dist_to_min_topsis = 0;


        
            foreach my $line (<MADM_TOPSIS>)
            {
                my @line_split = split ('\t',$line);
    
                $topsis_bs = sprintf('%s', $line_split[10]);
                my $topsis_bw = sprintf('%s', $line_split[4]);
                my $topsis_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $topsis_bw, $topsis_en;

                my $topsis_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $topsis_en = sprintf('%.2f', $line_split[2]);

                print "TOPSIS $topsis_bs    $topsis_bw    $topsis_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_topsis, $topsis_bs);

                    $count_topsis++;

                    my $energy_topsis_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_topsis_norm = ($topsis_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_topsis_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_topsis_norm = ($topsis_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_topsis_norm)**2 + ($bdw_topsis_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_topsis)
                    {
                        $min_dist_to_min_topsis = $dist_to_min;
                        
                        @min_bs_topsis = split(undef,$topsis_bs);
                        print "########################MIN TOPSIS\n";
                        
                    }

                    if ($dist_to_min > $max_dist_to_min_topsis)
                    {
                        $max_dist_to_min_topsis = $dist_to_min;
                    }
                }
            }

            close (MADM_TOPSIS);


            #count number of saw iter different solutions       
            @pareto_set = ();
        
            my $min_dist_to_min_saw_iter = 99999999;
            my $max_dist_to_min_saw_iter = 0;


        
            foreach my $line (<MADM_SAW_ITER>)
            {
                my @line_split = split ('\t',$line);
    
                $saw_iter_bs = sprintf('%s', $line_split[10]);
                my $saw_iter_bw = sprintf('%s', $line_split[4]);
                my $saw_iter_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $saw_iter_bw, $saw_iter_en;

                my $saw_iter_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $saw_iter_en = sprintf('%.2f', $line_split[2]);

                print "MADM ITER $saw_iter_bs    $saw_iter_bw    $saw_iter_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_saw_iter, $saw_iter_bs);

                    $count_saw_iter++;

                    my $energy_saw_iter_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_saw_iter_norm = ($saw_iter_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_saw_iter_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_saw_iter_norm = ($saw_iter_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_saw_iter_norm)**2 + ($bdw_saw_iter_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_saw_iter)
                    {
                        $min_dist_to_min_saw_iter = $dist_to_min;
                        
                        @min_bs_saw_iter = split(undef,$saw_iter_bs);
                        print "########################MIN SAW ITER\n";
                        
                    }

                    if ($dist_to_min > $max_dist_to_min_saw_iter)
                    {
                        $max_dist_to_min_saw_iter = $dist_to_min;
                    }
                }
            }

            close (MADM_SAW_ITER);

            #count number of mew iter different solutions       
            @pareto_set = ();
        
            my $min_dist_to_min_mew_iter = 99999999;
            my $max_dist_to_min_mew_iter = 0;


        
            foreach my $line (<MADM_MEW_ITER>)
            {
                my @line_split = split ('\t',$line);
    
                $mew_iter_bs = sprintf('%s', $line_split[10]);
                my $mew_iter_bw = sprintf('%s', $line_split[4]);
                my $mew_iter_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $mew_iter_bw, $mew_iter_en;

                my $mew_iter_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $mew_iter_en = sprintf('%.2f', $line_split[2]);

                print "MEW ITER $mew_iter_bs    $mew_iter_bw    $mew_iter_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_mew_iter, $mew_iter_bs);

                    $count_mew_iter++;

                    my $energy_mew_iter_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_mew_iter_norm = ($mew_iter_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_mew_iter_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_mew_iter_norm = ($mew_iter_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_mew_iter_norm)**2 + ($bdw_mew_iter_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_mew_iter)
                    {
                        $min_dist_to_min_mew_iter = $dist_to_min;
                        
                        @min_bs_mew_iter = split(undef,$mew_iter_bs);
                        print "########################MIN MEW ITER\n";
                        
                    }

                    if ($dist_to_min > $max_dist_to_min_mew_iter)
                    {
                        $max_dist_to_min_mew_iter = $dist_to_min;
                    }
                }
            }

            close (MADM_MEW_ITER);
            
            
            #count number of topsis iter different solutions       
            @pareto_set = ();
        
            my $min_dist_to_min_topsis_iter = 99999999;
            my $max_dist_to_min_topsis_iter = 0;


        
            foreach my $line (<MADM_TOPSIS_ITER>)
            {
                my @line_split = split ('\t',$line);
    
                $topsis_iter_bs = sprintf('%s', $line_split[10]);
                my $topsis_iter_bw = sprintf('%s', $line_split[4]);
                my $topsis_iter_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $topsis_iter_bw, $topsis_iter_en;

                my $topsis_iter_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $topsis_iter_en = sprintf('%.2f', $line_split[2]);

                print "TOPSIS ITER $topsis_iter_bs    $topsis_iter_bw    $topsis_iter_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_topsis_iter, $topsis_iter_bs);

                    $count_topsis_iter++;

                    my $energy_topsis_iter_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_topsis_iter_norm = ($topsis_iter_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_topsis_iter_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_topsis_iter_norm = ($topsis_iter_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_topsis_iter_norm)**2 + ($bdw_topsis_iter_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_topsis_iter)
                    {
                        $min_dist_to_min_topsis_iter = $dist_to_min;
                        
                        @min_bs_topsis_iter = split(undef,$topsis_iter_bs);
                        print "########################MIN TOPSIS ITER\n";
                        
                    }

                    if ($dist_to_min > $max_dist_to_min_topsis_iter)
                    {
                        $max_dist_to_min_topsis_iter = $dist_to_min;
                    }
                }
            }

            close (MADM_TOPSIS_ITER);
            
            
            #count number of gap different solutions       
            @pareto_set = ();
        
            my $min_dist_to_min_gap = 99999999;
            my $max_dist_to_min_gap = 0;


        
            foreach my $line (<GAP>)
            {
                my @line_split = split ('\t',$line);
    
                $gap_bs = sprintf('%s', $line_split[10]);
                my $gap_bw = sprintf('%s', $line_split[4]);
                my $gap_en = sprintf('%s', $line_split[2]);
            
                my $string = join "-", $gap_bw, $gap_en;

                my $gap_bw = sprintf('%.2f', ($line_split[4]/1000));
                my $gap_en = sprintf('%.2f', $line_split[2]);

                print "GAP $gap_bs    $gap_bw    $gap_en\n";

                if (&not_in_set($string) == 0)
                {
                    push (@pareto_set, $string);
                    push (@array_bs_gap, $gap_bs);

                    $count_gap++;

                    my $energy_gap_norm = 0;

                    if ($max_all_en - $min_all_en > 0)              
                    {           
                        $energy_gap_norm = ($gap_en - $min_all_en) / ($max_all_en - $min_all_en);
                    }

                    my $bdw_gap_norm = 0;
                
                    if ($max_all_bw - $min_all_bw > 0)              
                    {
                        $bdw_gap_norm = ($gap_bw - $min_all_bw) / ($max_all_bw - $min_all_bw);
                    } 


                    my $dist_to_min = (($energy_gap_norm)**2 + ($bdw_gap_norm)**2)**(0.5);
                    $sum_dist_to_min += $dist_to_min;

                    if ($dist_to_min < $min_dist_to_min_gap)
                    {
                        $min_dist_to_min_gap = $dist_to_min;
                        
                        @min_bs_gap = split(undef,$gap_bs);
                        print "########################MIN GAP\n";
                        
                    }

                    if ($dist_to_min > $max_dist_to_min_gap)
                    {
                        $max_dist_to_min_gap = $dist_to_min;
                    }
                }
            }

            close (GAP);

            print COUNTS_SOL "$gen\t$nb_chromo\t$count_mo\t$count_madm\t$count_mew\t$count_topsis\t$count_saw_iter\t$count_mew_iter\t$count_topsis_iter\t$count_gap\n";

            #difference between MADM and MO distance
            print "MIN_DIST_MO $min_dist_to_min_mo  MIN_DIST_MADM $min_dist_to_min_madm MIN_DIST_MEW $min_dist_to_min_mew\n";
            my $diff_min_dist = $min_dist_to_min_mo - $min_dist_to_min_madm;
            my $diff_min_dist_topsis = $min_dist_to_min_topsis - $min_dist_to_min_madm;
            my $diff_min_dist_saw_iter = $min_dist_to_min_saw_iter - $min_dist_to_min_madm;
            my $diff_min_dist_mew_iter = $min_dist_to_min_mew_iter - $min_dist_to_min_madm;
            my $diff_min_dist_topsis_iter = $min_dist_to_min_topsis_iter - $min_dist_to_min_madm;
            my $diff_min_dist_gap = $min_dist_to_min_gap - $min_dist_to_min_madm;
            
            my $diff_min_dist_mew = $min_dist_to_min_mo - $min_dist_to_min_mew;
            my $diff_min_dist_topsis_mew = $min_dist_to_min_topsis - $min_dist_to_min_mew;
            my $diff_min_dist_saw_iter_mew = $min_dist_to_min_saw_iter - $min_dist_to_min_mew;
            my $diff_min_dist_mew_iter_mew = $min_dist_to_min_mew_iter - $min_dist_to_min_mew;
            my $diff_min_dist_topsis_iter_mew = $min_dist_to_min_topsis_iter - $min_dist_to_min_mew;
            my $diff_min_dist_gap_mew = $min_dist_to_min_gap - $min_dist_to_min_mew;
            
            my $diff_min_dist_mo_topsis = $min_dist_to_min_mo - $min_dist_to_min_topsis;
            my $diff_min_dist_saw_iter_topsis = $min_dist_to_min_saw_iter - $min_dist_to_min_topsis;
            my $diff_min_dist_mew_iter_topsis = $min_dist_to_min_mew_iter - $min_dist_to_min_topsis;
            my $diff_min_dist_topsis_iter_topsis = $min_dist_to_min_topsis_iter - $min_dist_to_min_topsis;
            my $diff_min_dist_gap_topsis = $min_dist_to_min_gap - $min_dist_to_min_topsis;
            

            print DIFF_MADM_MO "$gen\t$nb_chromo\t$diff_min_dist\t$diff_min_dist_mew\t$diff_min_dist_topsis\t$diff_min_dist_topsis_mew\t$diff_min_dist_saw_iter\t$diff_min_dist_saw_iter_mew\t$diff_min_dist_mew_iter\t$diff_min_dist_mew_iter_mew\t$diff_min_dist_topsis_iter\t$diff_min_dist_topsis_iter_mew\t$diff_min_dist_gap\t$diff_min_dist_gap_mew\t$time_day\n";

            print DIFF_TOPSIS_MO "$gen\t$nb_chromo\t$diff_min_dist_mo_topsis\t$diff_min_dist_saw_iter_topsis\t$diff_min_dist_mew_iter_topsis\t$diff_min_dist_topsis_iter_topsis\t$diff_min_dist_gap_topsis\t$time_day\n";

            my $ratio_dist_to_min = $sum_dist_to_min / $count_dist_to_min;

            $min_dist_to_min_mo = sprintf("%.2f", $min_dist_to_min_mo);
            $max_dist_to_min_mo = sprintf("%.2f", $max_dist_to_min_mo);
            $ratio_dist_to_min = sprintf("%.2f", $ratio_dist_to_min);

            print RADIO_DIST_MIN "$gen\t$nb_chromo\t$ratio_dist_to_min\t$min_dist_to_min_mo\t$max_dist_to_min_mo\t$min_all_en $min_all_bw\n";
        
            print "ARRAY\n";
            print "@energy_dist\n";
            print "\n";
            print "@band_dist\n";
        
            my @index = 0..$#band_dist;
            @index = sort { $band_dist[$a] <=> $band_dist[$b] } @index;
            my @sorted_energy = @energy_dist[@index];
            my @sorted_band = @band_dist[@index];
        
            print "SORTED\n";
            print "@sorted_energy\n";
            print "\n";
            print "@sorted_band\n";

            my $max_dist = 0;
            my $sum_dist = 0;
            my $avg_dist = 0;
        

            for (my $i = 0; $i < scalar(@sorted_energy)-1; $i++)
            {
                my $dist = (($sorted_energy[$i+1] - $sorted_energy[$i])**2 + ($sorted_band[$i+1] - $sorted_band[$i])**2)**(0.5);

                if ($dist > $max_dist)
                {
                    $max_dist = $dist;
                }
        
                $sum_dist += $dist;         
            }

            $avg_dist = $sum_dist / scalar(@sorted_energy);

            print "MAX_DIST $max_dist   AVG_DIST $avg_dist\n";
            print DISTANCE_SOL "$gen\t$max_dist\t$avg_dist\n";



            #Count how many GENETIC solutions are "DOMINATED" for at least one MADM solution

            open OUTPUT_MO, "output.txt";
            open MADM, "madm";
            open MADM_MEW, "madm_mew";  
            open MADM_TOPSIS, "madm_topsis";
            open MADM_TOPSIS_ITER, "madm_topsis_iter";
            open MADM_MEW_ITER, "madm_mew_iter";
            open MADM_SAW_ITER, "madm_saw_iter";
            open GAP, "gap";
            open LAST_POP, "last_population_$gen";
            
            #open MADM_WEIGHT_NB_SOLS, "madm_weigth_nb_sols";
            #open MADM_WEIGHT_NB_SOLS_IMPROVED, "madm_weigth_nb_sols_improved";
            
            #open MADM_WEIGHT_DIST_SAW, "madm_weigth_dist_saw";
            #open MADM_WEIGHT_DIST_MEW, "madm_weigth_dist_mew";
            #open MADM_WEIGHT_DIST_TOPSIS, "madm_weigth_dist_topsis";


            foreach my $line (<OUTPUT_MO>)
            {

    
            }


            #Reallocation
            &number_reallocs ($event, $gen);
            #&number_reallocs_all ($event, $gen);
            

            my $result3 = `gnuplot plot_obj`;
            $result3 = `mkdir $time_day/$event-$gen`;
            $result3 = `mv objective.png $time_day/$event-$gen/objective_$event-$gen.png`;
            $result3 = `mv objective_iter.png $time_day/$event-$gen/objective_iter_$event-$gen.png`;
            $result3 = `mv output.txt $time_day/$event-$gen/output_$event-$gen.txt`;
            #$result3 = `mv old_output.txt $time_day/$event-$gen/old_output_$event-$gen.txt`;
            $result3 = `cp madm $time_day/$event-$gen/madm_$event-$gen.txt`;
            $result3 = `cp madm_mew $time_day/$event-$gen/madm_mew_$event-$gen.txt`;
            $result3 = `cp madm_topsis $time_day/$event-$gen/madm_topsis_$event-$gen.txt`;
            $result3 = `cp madm_topsis_iter $time_day/$event-$gen/madm_topsis_iter_$event-$gen.txt`;
            $result3 = `cp madm_mew_iter $time_day/$event-$gen/madm_mew_iter_$event-$gen.txt`;
            $result3 = `cp madm_saw_iter $time_day/$event-$gen/madm_saw_iter_$event-$gen.txt`;
            $result3 = `cp gap $time_day/$event-$gen/gap_$event-$gen.txt`;
            $result3 = `cp last_population_$gen $time_day/$event-$gen/last_population_$event-$gen.txt`;
            
            #$result3 = `cp madm_weigth_nb_sols $time_day/$event-$gen/madm_weigth_nb_sols_$event-$gen.txt`;
            #$result3 = `cp madm_weigth_nb_sols_improved $time_day/$event-$gen/madm_weigth_nb_sols_improved_$event-$gen.txt`;

            #$result3 = `cp madm_weigth_dist_saw $time_day/$event-$gen/madm_weigth_dist_saw_$event-$gen.txt`;
            #$result3 = `cp madm_weigth_dist_mew $time_day/$event-$gen/madm_weigth_dist_mew_$event-$gen.txt`;
            #$result3 = `cp madm_weigth_dist_topsis $time_day/$event-$gen/madm_weigth_dist_topsis_$event-$gen.txt`;
        
            if ($flag_all == 0)
            {
                $result3 = `cp all_solutions $time_day/$event-$gen/all_solutions_$event-$gen.txt`;
                $flag_all = 1;
            }

            $result3 = `mv time $time_day/$event-$gen/time_$event-$gen.txt`;
            #$result3 = `mv distance $time_day/$event/distance_$event.txt`;

            $result3 = `cp ../apps $time_day/$event-$gen/apps_$event-$gen`;
            $result3 = `cp ../interfaces $time_day/$event-$gen/interfaces_$event-$gen`;
            $result3 = `cp ../output_obj_energy $time_day/$event-$gen/output_obj_energy_$event-$gen`;
            $result3 = `cp ../output_sol $time_day/$event-$gen/output_obj_sol_$event-$gen`;
            $result3 = `cp ../reallocs $time_day/$event-$gen/reallocs_$event-$gen`;

            }

            
        }

        $first_sim = 0;
        $first_sim_all = 0;
    ###
    #}
    ###
    ###
    ###
    }
    #
}


print "WIN: $win_tot    LOOSE: $loose_tot   TOTAL: $nb_arrivals\n";

print WIN "WIN: $win_tot    LOOSE: $loose_tot   IND: $ind_tot   TOTAL: $nb_arrivals\n";
close(WIN);

#compute delay

foreach my $key (sort {$a<=>$b} keys %delay_calc)
{
    my $avg_mo = $delay_calc{$key}{'MO'} / $count_calc{$key}{'MO'};
    my $avg_all = $delay_calc{$key}{'ALL'} / $count_calc{$key}{'ALL'};

    print "APPS $key    MO $avg_mo  ALL $avg_all\n";
}

my $result4 = `mv ../output $time_day/output`;
close(FLOWGRAPH);
close(FLOW_FOR_ENER_CALC);
close(IFACES_FOR_ENER_CALC);
#close(IFACE_FLOW_SELECTION);
$result4 = `mv result_win $time_day/result_win`;

$result4 = `gnuplot ../plot_flowgraph`;
$result4 = `mv flows.eps $time_day/`;

$result4 = `gnuplot ../plot_bdwgraph`;
$result4 = `mv bandwidth.eps $time_day/`;

$result4 = `gnuplot ../plot_ifacesgraph`;
$result4 = `mv iface.eps $time_day/`;

$result4 = `perl plot_realocs.perl`;
$result4 = `mv plot_reallocs.eps $time_day/`;
$result4 = `mv ../reallocs $time_day/`;

$result4 = `perl plot_reallocs_all.perl`;
$result4 = `mv plot_reallocs_all.eps $time_day/`;
$result4 = `mv ../reallocs_all $time_day/`;

