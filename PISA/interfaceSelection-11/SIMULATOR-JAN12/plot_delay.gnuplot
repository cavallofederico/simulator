set terminal postscript color enhanced
set output "delay.eps"
set xlabel "Number of Applications"
set ylabel "Delay / s"
set log y
set key below
plot "global_delay_10.txt" u 1:2 with points lc 1 title "MO-10", "global_delay_25.txt" u 1:2 with points lc 1 title "MO-25", "global_delay_50.txt" u 1:2 with points lc 1 title "MO-50", "global_delay_75.txt" u 1:2 with points lc 1 title "MO-75", "global_delay_100.txt" u 1:2 with points lc 1 title "MO-100", "global_delay_250.txt" u 1:2 with points lc 1 title "MO-250", "global_delay_500.txt" u 1:2 with points lc 1 title "MO-500","global_delay_1000.txt" u 1:2 with points title "MO-1000","global_delay_2000.txt" u 1:2 with points lc 1 title "MO-2000", "global_delay_10.txt" u 1:4 with points ps 1 lc 3 title "MADM" 
