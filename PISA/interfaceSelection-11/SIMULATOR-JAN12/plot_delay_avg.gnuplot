set terminal postscript color enhanced
set output "delay_avg.eps"
set xlabel "Number of Applications"
set ylabel "Average Delay (s)"
set log y
set colorsequence classic
set key below
set xrange [0:13]
plot "delay_avg_10.txt" u 1:2 with points lc 1 ps 1.5 lw 3 title "g=10", "delay_avg_25.txt" u 1:2 with points lc 1 ps 1.5 lw 3 title "g=25", "delay_avg_50.txt" u 1:2 with points lc 1 ps 1.5 lw 3 title "g=50", "delay_avg_75.txt" u 1:2 with points lc 1 ps 1.5 lw 3 title "g=75", "delay_avg_100.txt" u 1:2 with points lc 1 ps 1.5 lw 3 title "g=100", "delay_avg_250.txt" u 1:2 with points lc 1 ps 1.5 lw 3 title "g=250", "delay_avg_10.txt" u 1:4 with points ps 1.5 lw 3 lc 3 title "MADM", "delay_avg_10.txt" u 1:5 with points lc 9 pt 9 ps 1.5 lw 3 title "MADM p/ APP","delay_avg_10.txt" u 1:6 with points pt 12 ps 1.5 lw 3 lc 5 title "GAP"
