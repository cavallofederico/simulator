#!/usr/bin/perl


#my @gen = (10,25,50,75,100,250,500,1000,2000);
my @gen = (10,25,50,75,100,250);

%hash_mo = ();
%hash_all = ();
%hash_madm = ();
%hash_madm_it = ();
%hash_gap = ();
%hash_count = ();


foreach my $item (@gen)
{
	open FILE, "global_delay_$item.txt";

	print "FILE OPENED\n";

	foreach my $line (<FILE>)
	{
		my @array = split ('\t',$line);
		print $line;
		
		if ($array[0]>0)
		{
			$hash_mo{$item}{$array[0]}+=$array[1];
			$hash_all{$item}{$array[0]}+=$array[2];
			$hash_madm{$item}{$array[0]}+=$array[3];
			$hash_madm_it{$item}{$array[0]}+=$array[4];
			$hash_gap{$item}{$array[0]}+=$array[5];


			$hash_count{$item}{$array[0]}++;
		}
	}

	close FILE;
}



open AMOUNT_SAMPLES, ">amoun_of_samples.txt";

foreach my $key (sort {$a<=>$b} keys %hash_count)
{
	open OUT, ">delay_avg_$key.txt";

	foreach my $chrom (sort {$a<=>$b} keys %{$hash_count{$key}})
	{
		my $avg_mo = $hash_mo{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_all = $hash_all{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_madm = $hash_madm{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_madm_it = $hash_madm_it{$key}{$chrom} / $hash_count{$key}{$chrom};
		my $avg_gap = $hash_gap{$key}{$chrom} / $hash_count{$key}{$chrom};

		if ($chrom%2 == 0)
		{
            my $nb_apps = $chrom / 2;
			print OUT "$nb_apps\t$avg_mo\t$avg_all\t$avg_madm\t$avg_madm_it\t$avg_gap\n";	
			print AMOUNT_SAMPLES "$nb_apps\t$hash_count{$key}{$chrom}\n";
		}
	}
	close OUT;
}

close AMOUNT_SAMPLES;

