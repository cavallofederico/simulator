set terminal postscript color enhanced
set output "plot_avg_reallocs_all.eps"
set xlabel "Event"
set ylabel "Cumulative average number of reallocations"
set colorsequence classic
set key below
set grid y
plot "reallocs_avg_global" u 1:17 with points ps 1.5 lc 1 lw 3 title "g=10", "reallocs_avg_global" u 1:19 with points ps 1.5 lc 1 lw 3 title "g=25", "reallocs_avg_global" u 1:21 with points ps 1.5 lc 1 lw 3 title "g=50", "reallocs_avg_global" u 1:23 with points pt 13 ps 1.5 lc 1 lw 3 title "g=75", "reallocs_avg_global" u 1:25 with points pt 4 ps 1.5 lc 1 lw 3 title "g=100","reallocs_avg_global" u 1:27 with points pt 5 ps 1.5 lc 1 lw 3 title "g=250","reallocs_avg_global" u 1:3 with points pt 6 ps 1.5 lc 3 lw 2 title "SAW","reallocs_avg_global" u 1:5 with points pt 7 ps 1.5 lc 3 lw 2 title "MEW","reallocs_avg_global" u 1:7 with points pt 8 ps 1.5 lc 3 lw 2 title "TOPSIS","reallocs_avg_global" u 1:9 with points pt 9 ps 1.5 lc 9 lw 2 title "SAW p/ APP","reallocs_avg_global" u 1:11 with points pt 10 ps 1.5 lc 9 lw 2 title "MEW p/APP","reallocs_avg_global" u 1:13 with points pt 11 ps 1.5 lc 9 lw 2 title "TOPSIS p/APP","reallocs_avg_global" u 1:15 with points pt 12 ps 1.5 lc 5 lw 2 title "GAP"
