/*========================================================================
  PISA  (www.tik.ee.ethz.ch/pisa/)
 
  ========================================================================
  Computer Engineering (TIK)
  ETH Zurich
 
  ========================================================================
  PISALIB 
  
  Pisa basic functions for use in variator_user.c
  
  Header file.
  
  file: variator.h
  author: German Castignani, german@castignani.com.ar, TELECOM Bretagne

  revision by: 
  last change: $date$
  
  ========================================================================
*/


#ifndef VARIATOR_USER_H
#define VARIATOR_USER_H

#define PISA_UNIX /**** replace with PISA_WIN if compiling for Windows */

/* maximal length of filenames */
#define FILE_NAME_LENGTH 128  /**** change the value if you like */

/* maximal length of entries in local cfg file */
#define CFG_NAME_LENGTH 128   /**** change the value if you like */

#define MAX_IFACES 10

#define TIMER 15

#define CHANNELS 13

#define CHANNEL_SWITCH 0.003 //3ms channel switch delay

/*---| declaration of global variables (defined in variator_user.c) |-----*/

extern char *log_file; /* file to log to */

extern char paramfile[]; /* file with local parameters */

extern char ifaces_file[];

extern char apps_file[];


/*-----------------------------------------------------------------------*/

struct individual_t
{
     /**********| added for ifacesel |*****************/
     int *bit_string; /* the genes for the interfaces coded in 7 digits each one */
     int length;      /* length of the bit_strings */
     double energy;
     double bandwidth;
     double * TI;
     double * TB;
     double * energy_array;
     /**********| addition for ifacesel end |**********/
};


struct interface_t
{
    int type; // 0:CELL 1:WLAN
    int av_bdw;
    int pow1; // DCH/TX
    int pow2; // FACH/RX
    int pow3; // PCH / IDLE
    //int pow4; // SLEEP
    int timer1; // T1/Tsleep
    int timer2; // T2
};


/*struct app_t
{
    int type; // 0:WEB 1:BKG 2:REALTIME
    int db; // duree du burst
    int ti; // interval entre burst
    int sb; // taille du burst
};*/

struct app_t
{
    int size; //
    float bdw;
    float time;
};

/*-------------------| functions for individual struct |----------------*/

void free_individual(individual *ind);
/* Frees the memory for one indiviual.

   post: memory for ind is freed
*/


double get_objective_value(int identity, int i); 
/* Gets objective value of an individual.

   pre: 0 <= i <= dim - 1 (dim is the number of objectives)

   post: Return value == the objective value number 'i' in individual '*ind'.
         If no individual with ID 'identity' return value == -1. 
*/   

/*-------------------------| statemachine |-----------------------------*/


int state0(); 
/* Do what needs to be done in state 0.

   pre: The global variable 'paramfile' contains the name of the
        parameter file specified on the commandline.
        The global variable 'alpha' contains the number of indiviuals
        you need to generate for the initial population.
                
   post: Optionally read parameter specific for the module.
         Optionally do some initialization.
         Initial population created.
         Information about initial population written to the ini file
         using write_ini().
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/


int state2();
/* Do what needs to be done in state 2.

   pre: The global variable 'mu' contains the number of indiviuals
        you need to read using 'read_sel()'.
        The global variable 'lambda' contains the number of individuals
        you need to create by variation of the individuals specified the
        'sel' file.
        
   post: Optionally call read_arc() in order to delete old uncessary
         individuals from the global population.
         read_sel() called
         'lambda' children generated from the 'mu' parents
         Children added to the global population using add_individual().
         Information about children written to the 'var' file using
         write_var().
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/


int state4();
/* Do what needs to be done in state 4.

   pre: State 4 means the variator has to terminate.

   post: Free all memory.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/


int state7();
/* Do what needs to be done in state 7.

   pre: State 7 means that the selector has just terminated.

   post: You probably don't need to do anything, just return 0.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/


int state8();
/* Do what needs to be done in state 8.

   pre: State 8 means that the variator needs to reset and get ready to
        start again in state 0.

   post: Get ready to start again in state 0, this includes:
         Free all memory.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/


int state11();
/* Do what needs to be done in state 11.

   pre: State 11 means that the selector has just reset and is ready
        to start again in state 1.

   post: You probably don't need to do anything, just return 0.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/

int is_finished();
/* Tests if ending criterion of your algorithm applies.

   post: return value == 1 if optimization should stop
         return value == 0 if optimization should continue
*/

/**********| added for ifacesel |*****************/
int read_input();

int read_local_parameters();
/* read local parameters from file */

individual *new_individual();
/* Creates a random new individual and allocates memory for it.
   Returns a pointer to the new individual.
   Returns NULL if allocation failed.*/

individual *copy_individual(individual* ind);
/* Allocates memory for a new individual and copies 'ind' to this place.
   Returns the pointer to new individual.
   Returns NULL if allocation failed. */

int variate(int *parents, int *offspring);
/* Performs variation (= recombination and mutation) according to
   settings in the parameter file.
   Returns 0 if successful and 1 otherwise.*/

int one_bit_mutation(individual *ind);
/* Toggles exactly one bit in 'ind'. */

int indep_bit_mutation(individual *ind);
/* Toggles each bit with probability bit_turn_prob/ind.lenght. */

int one_point_crossover(individual *ind1, individual *ind2);
/* Performs "one point crossover".
   Takes pointers to two individuals as arguments and replaces them by the
   children (i.e. the children are stored in ind1 and ind2). */

int uniform_crossover(individual *ind1, individual *ind2);
/* Performs a "uniform crossover".
   Takes pointers to two individuals as arguments and replaces them by the
   children (i.e. the children are stored in ind1 and ind2). */

int irand(int range);
/* Generate a random integer. */

double drand(double range);
/* Generate a random double. */

double eval_energy(individual *ind);
/* Returns the objective value of 'ind' based on Leading Ones.
   In order to maximize Leading Ones this function is minimized.
   PISA always minimizes. */

double eval_bandwidth(individual *ind);
/* Returns the objective value of 'ind' based on Trailing Zeros.
   In order to maximize Trailing Zeros this function is minimized.
   PISA always minimizes. */

void write_output_file();
/* Writes the index, objective values and bit string of
   all individuals in global_population to 'out_filename'. */

/**********| addition for ifacesel end |**********/


#endif /* VARIATOR_USER.H */
