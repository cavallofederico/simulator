/*========================================================================
  PISA  (www.tik.ee.ethz.ch/pisa/)
 
  ========================================================================
  Computer Engineering (TIK)
  ETH Zurich
 
  ========================================================================
  PISALIB 
  
  Pisa basic functions for use in variator_user.c
  
  Header file.
  
  file: variator.h
  author: German Castignani, german@castignani.com.ar, TELECOM Bretagne

  revision by: 
  last change: $date$
  
  ========================================================================
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>


#include "variator.h"
#include "variator_user.h"

/*--------------------| global variable definitions |-------------------*/

/*==== declared in variator_user.h used in other files as well ====*/

char *log_file = "ifaceselection11_error.log"; /**** changed for ifacesel */

char paramfile[FILE_NAME_LENGTH]; /* file with local parameters */

char ifaces_file[FILE_NAME_LENGTH];

char apps_file[FILE_NAME_LENGTH];

/*==== only used in this file ====*/

/* local parameters from paramfile*/
unsigned int seed;   /* seed for random number generator */

int length; /* length of the binary string */

int len_iface;

int maxgen; /* maximum number of generations (stop criterion) */

int gen;

char outfile[FILE_NAME_LENGTH]; /* output file for last population */

int mutation_type; /* 0 = no mutation
		      1 = one bit mutation
		      2 = independent bit mutation */

int recombination_type; /* 0 = no recombination
			   1 = one point crossover
			   2 = uniform crossover */

double mutat_prob; /* probability that individual is mutated */

double recom_prob;  /* probability that 2 individual are recombined */

double bit_turn_prob;

/*double latency_input[13][20][20];
double failure_input[13][20][20];
double discovery_input[13][20][20];*/

struct interface_t array_ifaces[10];
struct app_t array_apps[20];

int nb_ifaces;
int nb_apps;

double total_bdw = 0;
double total_dem = 0;



/* probability that bit is turned when mutation occurs, only used for
 * independent bit mutation. */


void solution_bin_to_dec(int* ind, int* array)
{
	int i;
	int value;

	for (i=0;i<nb_apps;i++)
	{
		value = bin_dec(ind, i*len_iface);
		array[i]=value;
	}

}

void dec_bin(int number, int* binary) {
	int x, y;
	int start, position;
	x = y = 0;

	start = len_iface - 1;

	for(y = start; y >= 0; y--) {
		x = number / (1 << y);
		if (x>1)
			printf("ERROR");
		number = number - x * (1 << y);
		position = (start-y);
		binary[position]= x;
	}
}

void dec_bin_long(int number, int* binary) {
	int x, y;
	int start, position;
	x = y = 0;

	start = length - 1;

	for(y = start; y >= 0; y--) {
		x = number / (1 << y);
		if (x>1)
			printf("ERROR");
		number = number - x * (1 << y);
		position = (start-y);
		binary[position]= x;
	}
}

int bin_dec(int* binary, int offset)
{
	int aux = 0;
	int y;

	for (y = 0; y < len_iface ; y++)
	{
		aux += binary[y+offset]*pow(2,(len_iface - 1 - y));
	}

	return(aux);
}

int check_integrity(int* binary)
{
	int i, iface_sol, iface_rand, j;
	int *binary_iface;
	int flag = 0;

	for (i=0;i<nb_apps;i++)
	{
		iface_sol = bin_dec(binary,len_iface*i);

		if ((iface_sol >= nb_ifaces)) // If the interface is not valid
		{

			return(1);
			/*
			binary_iface = (int *) malloc(sizeof(int) * len_iface);
	    	iface_rand = irand(nb_ifaces); // chose a random new interface

	    	dec_bin(iface_rand, binary_iface);

	    	for (j = 0; j < len_iface; j++)
	    	{
	    		binary[(len_iface*i)+j]=binary_iface[j];
	    	}
	    	*/
		}
	}

	return(0);
}



/*-------------------------| individual |-------------------------------*/

void free_individual(individual *ind) 
/* Frees the memory for one indiviual.

   post: memory for ind is freed
*/
{
     /**********| added for ifacesel |*****************/

     if (ind == NULL)
          return;

     free(ind->bit_string);
     //free(ind->TB);
     //free(ind->TI);

     /**********| addition for ifacesel end |**********/

     free(ind);
}

double get_objective_value(int identity, int i)
/* Gets objective value of an individual.

   pre: 0 <= i <= dimension - 1 (dimension is the number of objectives)

   post: Return value == the objective value number 'i' in individual '*ind'.
         If no individual with ID 'identity' return value == -1. 
*/   
{
     /**********| added for ifacesel |*****************/
     individual *temp_ind;
     /**********| addition for ifacesel end |**********/

     double objective_value = -1.0;
     
     assert(0 <= i && i < dimension); /* asserting the pre-condition GERMAN: dim+1*/
     
     /**********| added for ifacesel |*****************/
     if(i < 0 || i > (dimension - 1))
          return (-1);
     
     temp_ind = get_individual(identity);
     if(temp_ind == NULL)
          return (-1);
     
     if(i == 0)
          objective_value = temp_ind->energy;
     else if (i == 1)
          objective_value = temp_ind->bandwidth;
     
     /**********| addition for ifacesel end |**********/
     
     return (objective_value);
}

/*-------------------------| statemachine functions |-------------------*/

int state0() 
/* Do what needs to be done in state 0.

   pre: The global variable 'paramfile' contains the name of the
        parameter file specified on the commandline.
        The global variable 'alpha' contains the number of indiviuals
        you need to generate for the initial population.
                
   post: Optionally read parameter specific for the module.
         Optionally do some initialization.
         Initial population created.
         Information about initial population written to the ini file
         using write_ini().
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/
{
     /**********| added for ifacesel |*****************/
     int i;
     /**********| addition for ifacesel end |**********/
     
     int result; /* stores return values of called functions */
     int *initial_population; /* storing the IDs of the individuals */
     initial_population = (int *) malloc(alpha * sizeof(int)); 
     if (initial_population == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (1);
     }
     
     /**********| added for ifacesel |*****************/
     result = read_local_parameters();  

     if (result != 0)
     { 
          log_to_file(log_file, __FILE__, __LINE__,
                      "couldn't read local parameters");
          return (1);
     }
     
     result = read_input();

     if (result != 0)
          {
               log_to_file(log_file, __FILE__, __LINE__,
                           "couldn't read input");
               return (1);
          }

     /* create first alpha individuals */
     for(i = 0; i < alpha; i++)
     {
          initial_population[i] = add_individual(new_individual());
          if(initial_population[i] == -1)
               return (1);
     } 

     gen = 1;

     /**********| addition for ifacesel end |**********/

     result = write_ini(initial_population);
     if (result != 0)
     { 
          log_to_file(log_file, __FILE__, __LINE__,
                      "couldn't write ini");
          free(initial_population);
          return (1);
     }

    free(initial_population);
     return (0);
}



int state2()
/* Do what needs to be done in state 2.

   pre: The global variable 'mu' contains the number of indiviuals
        you need to read using 'read_sel()'.
        The global variable 'lambda' contains the number of individuals
        you need to create by variation of the individuals specified the
        'sel' file.
        
   post: Optionally call read_arc() in order to delete old uncessary
         individuals from the global population.
         read_sel() called
         'lambda' children generated from the 'mu' parents
         Children added to the global population using add_individual().
         Information about children written to the 'var' file using
         write_var().
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/
{
     int *parent_identities, *offspring_identities; /* array of identities */
     int result; /* stores return values of called functions */

     parent_identities = (int *) malloc(mu * sizeof(int)); 
     if (parent_identities == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (1);
     }

     offspring_identities = (int *) malloc(lambda * sizeof(int)); 
     if (offspring_identities == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (1);
     }
     
     result = read_sel(parent_identities);
     if (result != 0) /* if some file reading error occurs, return 2 */
          return (2);

     result = read_arc(); 
     if (result != 0) /* if some file reading error occurs, return 2 */
          return (2);
     
     /**********| added for ifacesel |*****************/
     
     result = variate(parent_identities, offspring_identities);
     if (result != 0)
          return (1);
          
     gen++;

     /**********| addition for ifacesel end |**********/

     result = write_var(offspring_identities);
     if (result != 0)
     { 
          log_to_file(log_file, __FILE__, __LINE__,
                      "couldn't write var");
          free(offspring_identities);
          free(parent_identities);
          return (1);
     }

     free(offspring_identities);
     free(parent_identities);
     return (0);
}
 

int state4() 
/* Do what needs to be done in state 4.

   pre: State 4 means the variator has to terminate.

   post: Free all memory.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/
{
     /**********| added for ifacesel |*****************/
     
     int result, i,j;
     result = read_arc();

    /* for (i=0; i<=13;i++)
     {
    	 for (j=0; j<=20;j++)
    	 {
    		 free(latency_input[i][j]);
    		 free(failure_input[i][j]);
    		 free(discovery_input[i][j]);
    	 }

    	 free(latency_input[i]);
    	 free(failure_input[i]);
    	 free(discovery_input[i]);
     }


	 free(latency_input);
	 free(failure_input);
	 free(discovery_input);
*/
     if (0 == result) /* arc file correctly read
                         this means it was not read before,
                         e.g., in a reset. */
     {
        write_output_file();
     }

     /**********| addition for ifacesel end |**********/
     
     return (0);
}


int state7()
/* Do what needs to be done in state 7.

   pre: State 7 means that the selector has just terminated.

   post: You probably don't need to do anything, just return 0.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/
{
     return(0);  
}


int state8()
/* Do what needs to be done in state 8.

   pre: State 8 means that the variator needs to reset and get ready to
        start again in state 0.

   post: Get ready to start again in state 0. 
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/
{
     /**********| added for ifacesel |*****************/

   int result;
   
   gen = 1;
     
   result = read_arc();

   if (0 == result) /* arc file correctly read
                       this means it was not read before */
   {
      write_output_file();
   }

     
     /**********| addition for ifacesel end |**********/
     
     return (0);
}


int state11()
/* Do what needs to be done in state 11.

   pre: State 11 means that the selector has just reset and is ready
        to start again in state 1.

   post: You probably don't need to do anything, just return 0.
         Return value == 0 if successful,
                      == 1 if unspecified errors happened,
                      == 2 if file reading failed.
*/
{
     return (0);  
}


int is_finished()
/* Tests if ending criterion of your algorithm applies.

   post: return value == 1 if optimization should stop
         return value == 0 if optimization should continue
*/
{
     /**********| added for ifacesel |*****************/
     return (gen >= maxgen);
     /**********| addition for ifacesel end |**********/
}

/**********| added for ifacesel |*****************/

int read_input()
{

	FILE *fp_ifaces;
	FILE *fp_apps;
    int result, i, flag, aux;
    int order;
    char str[CFG_NAME_LENGTH];


    order = 0;
    i= 1;
    flag = 0;

    /* reading parameter file with parameters for selection */
    /* READ INTERFACES FILE */

    fp_ifaces = fopen(ifaces_file, "r");
    assert(fp_ifaces != NULL);

   /* while (result != EOF)
	{
    	result = fscanf(fp_ifaces, "%s", str);

    	if (strcmp(str, "iface")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &order);
    	    //array_ifaces[order] = (struct interface_t *) malloc(sizeof(interface_t));

    	} else if (strcmp(str, "av_bdw")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].av_bdw);
    		total_bdw += array_ifaces[order].av_bdw;

    	} else if (strcmp(str, "type")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].type);

    	} else if (strcmp(str, "pow1")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].pow1);

      	} else if (strcmp(str, "pow2")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].pow2);

      	} else if (strcmp(str, "pow3")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].pow3);

      	} else if (strcmp(str, "pow4")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].pow4);
      	} else if (strcmp(str, "timer1")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].timer1);
      	} else if (strcmp(str, "timer2")==0)
    	{
    		result = fscanf(fp_ifaces, "%d", &array_ifaces[order].timer2);
      	}

	}*/

    while (result != EOF)
	{
		result = fscanf(fp_ifaces, "%s", str);

		if (strcmp(str, "iface")==0)
		{
			result = fscanf(fp_ifaces, "%d", &aux);
			nb_ifaces++;
			order++;
			//array_ifaces[order] = (struct interface_t *) malloc(sizeof(interface_t));

		} else if (strcmp(str, "type")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].type);

		} else if (strcmp(str, "av_bdw")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].av_bdw);
			total_bdw += array_ifaces[order-1].av_bdw;

		/*} else if (strcmp(str, "type")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order].type);
		 */
		} else if (strcmp(str, "pow1")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].pow1);

		} else if (strcmp(str, "pow2")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].pow2);

		} else if (strcmp(str, "pow3")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].pow3);

		/*} else if (strcmp(str, "pow4")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order].pow4);
		*/
		} else if (strcmp(str, "timer1")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].timer1);
		} else if (strcmp(str, "timer2")==0)
		{
			result = fscanf(fp_ifaces, "%d", &array_ifaces[order-1].timer2);
		}

	}

    total_bdw = (double) total_bdw;

    //nb_ifaces = order + 1;
    result= 0;
    order = 0;

    /* READ APPLICATIONS FILE */

    fp_apps = fopen(apps_file, "r");
    assert(fp_apps != NULL);


   /* while (result != EOF)
	{
    	result = fscanf(fp_apps, "%s", str);

    	if (strcmp(str, "app")==0)
    	{
    		result = fscanf(fp_apps, "%d", &order);
    	    //array_apps[order] = (struct app_t *) malloc(sizeof(app_t));

    	} else if (strcmp(str, "type")==0)
    	{
    		result = fscanf(fp_apps, "%d", &array_apps[order].type);

    	} else if (strcmp(str, "db")==0)
    	{
    		result = fscanf(fp_apps, "%d", &array_apps[order].db);

      	} else if (strcmp(str, "ti")==0)
    	{
    		result = fscanf(fp_apps, "%d", &array_apps[order].ti);

      	} else if (strcmp(str, "sb")==0)
    	{
    		result = fscanf(fp_apps, "%d", &array_apps[order].sb);
    		total_dem += (double) array_apps[order].sb*1000/(array_apps[order].ti+array_apps[order].db);
      	}

    	strcpy(str,"0");
	}
*/

    while (result != EOF)
	{
		result = fscanf(fp_apps, "%s", str);

		if (strcmp(str, "app")==0)
		{
			result = fscanf(fp_apps, "%d", &aux);
			nb_apps ++;
			order ++;
			//array_apps[order] = (struct app_t *) malloc(sizeof(app_t));

		} else if (strcmp(str, "size")==0)
		{
			result = fscanf(fp_apps, "%d", &array_apps[order-1].size);
			//total_dem += (double) array_apps[order].sb*1000/(array_apps[order].ti+array_apps[order].db);

		} else if (strcmp(str, "bdw")==0)
		{
			result = fscanf(fp_apps, "%f", &array_apps[order-1].bdw);
			//total_dem += (double) array_apps[order].sb*1000/(array_apps[order].ti+array_apps[order].db);
		} else if (strcmp(str, "time")==0)
		{
			result = fscanf(fp_apps, "%f", &array_apps[order-1].time);
		}

		strcpy(str,"0");
	}
    //printf("%f",total_dem);

   //++ nb_apps = order + 1;

    while (!flag)
    {
    	if (pow(2,i)<nb_ifaces)
    		i++;
    	else
    		flag = 1;
    }

    length = nb_apps * i;
    len_iface = i;

    return(0);

}

int read_local_parameters()
{
     FILE *fp;
     int result, i;
     char str[CFG_NAME_LENGTH];
     
     /* reading parameter file with parameters for selection */
     fp = fopen(paramfile, "r"); 
     assert(fp != NULL);

     if(dimension > 3 || dimension < 0)
     {
          log_to_file(log_file, __FILE__, 
                      __LINE__, "can't handle that dimension");
          return(1);
     } 

     if(mu != lambda)
     {
          log_to_file(log_file, __FILE__, 
                      __LINE__, "can't handle mu != lambda");
          return(1);
     }


     fscanf(fp, "%s", str);
     assert(strcmp(str, "seed") == 0);
     fscanf(fp, "%u", &seed); /* %u because seed is unsigned */

     //fscanf(fp, "%s", str);
     //assert(strcmp(str, "length") == 0);
     //fscanf(fp, "%d", &length);

     fscanf(fp, "%s", str);
     assert(strcmp(str, "maxgen") == 0);
     fscanf(fp, "%d", &maxgen);
     
     fscanf(fp, "%s", str);
     assert(strcmp(str, "outputfile") == 0);
     fscanf(fp, "%s", outfile); /* fscanf() returns EOF if
                                   reading failed. */
     fscanf(fp, "%s", str);
     assert(strcmp(str, "mutation_type") == 0);
     fscanf(fp, "%d", &mutation_type);
    
     fscanf(fp, "%s", str);
     assert(strcmp(str, "recombination_type") == 0);
     fscanf(fp, "%d", &recombination_type);

     fscanf(fp, "%s", str);
     assert(strcmp(str, "mutation_probability") == 0);
     fscanf(fp, "%le", &mutat_prob);

     fscanf(fp, "%s", str);
     assert(strcmp(str, "recombination_probability") == 0);
     fscanf(fp, "%le", &recom_prob);

     fscanf(fp, "%s", str);
     assert(strcmp(str, "bit_turn_probability") == 0);
     result = fscanf(fp, "%le", &bit_turn_prob);

     assert(result != EOF); /* no EOF, outfile correctly read */
     
     srand(seed); /* seeding random number generator */

     fclose(fp);
     
     return (0);
}


int variate(int *parents, int *offspring)
/* Performs variation (= recombination and mutation) according to
   settings in the parameter file.
   Returns 0 if successful and 1 otherwise.*/
{
     int result, i, k;

     result = 1;

     /* copying all individuals from parents */
     for(i = 0; i < mu; i++)
     {
          offspring[i] = 
               add_individual(copy_individual(get_individual(parents[i])));
          if(offspring[i] == -1)
          {
               log_to_file(log_file, __FILE__, __LINE__,
                           "copying + adding failed");
               return (1);
          }
     }
 
     /* if odd number of individuals, last one is
        left as is */
     if((((double)mu/2) - (int)(mu/2)) != 0)
          k = mu - 1; 
     else
          k = mu;

     /* do recombination */
     for(i = 0; i < k; i+= 2)
     {  
          result = 1;
          if (drand(1) <= recom_prob)
          {
               result = 1;
               if (recombination_type == 1)
               {
                    result = one_point_crossover(get_individual(offspring[i]),get_individual(offspring[i + 1]));
               }
               else if (recombination_type == 2)
               {
                    result = uniform_crossover(get_individual(offspring[i]), get_individual(offspring[i + 1]));
               }
               else if (recombination_type == 0)
                    result = 0;

               if (result != 0)
               {
                    log_to_file(log_file, __FILE__, 
                                __LINE__, "recombination failed!");
                    return (1);
               }
          }
     }

     /* do mutation */
     for(i = 0; i < mu; i++)
     {
          result = 1;
          if (drand(1) <= mutat_prob) /* only mutate with mut.probability */
          { 
               if(mutation_type == 1)
               {
                    result = one_bit_mutation(get_individual(offspring[i]));
               }
               else if(mutation_type == 2)
               {
                    result = indep_bit_mutation(get_individual(offspring[i]));
               }
               else if(mutation_type == 0)
               {
                    result = 0;
               }
    
               if(offspring[0] == -1)
               {
                    log_to_file(log_file, __FILE__, __LINE__,
                                "mutation failed!");
                    return (1);
               }
          }
     }
     
     return (0);
}



int one_bit_mutation(individual *ind)
/* Toggles exactly one bit in 'ind'. */
{
     int position;
     int integrity;
     int *bit_string_temp1;
     int i;

     bit_string_temp1 = (int *) malloc(sizeof(int) * ind->length);


     if(ind == NULL)
          return (1);
     /* flip bit at position */
     position = irand(ind->length);
     
     /*
     if(ind->bit_string[position] == 0)
          ind->bit_string[position] = 1;
     else
          ind->bit_string[position] = 0;
	 */

     for(i = 0; i < ind->length; i++)
     {
    	 bit_string_temp1[i] = ind->bit_string[i];
     }

     if(bit_string_temp1[position] == 0)
    	 bit_string_temp1[position] = 1;
     else
    	 bit_string_temp1[position] = 0;

     integrity = check_integrity(bit_string_temp1);

     if (integrity == 0)
     {
    	 for(i = 0; i < ind->length; i++)
    	 {
    		 ind->bit_string[i] = bit_string_temp1[i];
    	 }

    	 ind->energy = eval_energy(ind);
    	 ind->bandwidth = eval_bandwidth(ind);
     }

     free(bit_string_temp1);
     return (0);
}


int indep_bit_mutation(individual *ind)
/* Toggles each bit with probability bit_turn_prob */
{
     int i;
     double probability;
     int *bit_string_temp1;
     int integrity;

     bit_string_temp1 = (int *) malloc(sizeof(int) * ind->length);

     for(i = 0; i < ind->length; i++)
     {
    	 bit_string_temp1[i] = ind->bit_string[i];
     }


     /* absolute probability
     probability = bit_turn_prob;
     if(ind == NULL)
          return (1);

     for(i = 0; i < ind->length; i++)
     {
          if(drand(1) < probability)
          {
               /* flip bit at position i*/ /*
               if(ind->bit_string[i] == 0)
                    ind->bit_string[i] = 1;
               else
                    ind->bit_string[i] = 0;
          }
     }

     */

     /* absolute probability */
	 probability = bit_turn_prob;

	 for(i = 0; i < ind->length; i++)
	 {
		  if(drand(1) < probability)
		  {
			   /* flip bit at position i*/
			   if(bit_string_temp1[i] == 0)
				   bit_string_temp1[i] = 1;
			   else
				   bit_string_temp1[i] = 0;
		  }
	 }

     integrity = check_integrity(bit_string_temp1);

     if (integrity == 0)
          {
         	 for(i = 0; i < ind->length; i++)
         	 {
         		 ind->bit_string[i] = bit_string_temp1[i];
         	 }

         	 ind->energy = eval_energy(ind);
         	 ind->bandwidth = eval_bandwidth(ind);
          }

     free(bit_string_temp1);

     return (0);
}


int one_point_crossover(individual *ind1, individual *ind2)
/* Performs "one point crossover".
   Takes pointers to two individuals as arguments and replaces them by the
   children (i.e. the children are stored in ind1 and ind2). */

{
	 int integrity;
	 int *bit_string_temp1;
	 int *bit_string_temp2;

     int app_offset, position, i, j;
     int *bit_string_ind2;
     bit_string_ind2 = (int *) malloc(sizeof(int) * ind2->length);
     bit_string_temp1 = (int *) malloc(sizeof(int) * ind1->length);
     bit_string_temp2 = (int *) malloc(sizeof(int) * ind2->length);

     if (bit_string_ind2 == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (1);
     }

     /*
     for(i = 0; i < ind2->length; i++)
     {
          bit_string_ind2[i] = ind2->bit_string[i];
     }*/

     for(i = 0; i < ind2->length; i++)
     {
          bit_string_ind2[i] = ind2->bit_string[i];
          bit_string_temp1[i] = ind1->bit_string[i];
          bit_string_temp2[i] = ind2->bit_string[i];
     }


     app_offset = irand(nb_apps);

     position = app_offset * len_iface;

     /*
	  for(i = 0; i < position; i++) {
		   ind2->bit_string[i] = ind1->bit_string[i];
		   ind1->bit_string[i] = bit_string_ind2[i];
	  }
      */

     for(i = 0; i < position; i++) {
    	 	 bit_string_temp2[i] = ind1->bit_string[i];
    	 	 bit_string_temp1[i] = bit_string_ind2[i];
      }


     free(bit_string_ind2);

     integrity = check_integrity(bit_string_temp1);

     if (integrity == 0)
	  {
		 for(i = 0; i < ind1->length; i++)
		 {
			 ind1->bit_string[i] = bit_string_temp1[i];
		 }

		 ind1->energy = eval_energy(ind1);
		 ind1->bandwidth = eval_bandwidth(ind1);
      }

     integrity = check_integrity(bit_string_temp2);

     if (integrity == 0)
	  {
		 for(i = 0; i < ind2->length; i++)
		 {
			 ind2->bit_string[i] = bit_string_temp2[i];
		 }

		 ind2->energy = eval_energy(ind2);
		 ind2->bandwidth = eval_bandwidth(ind2);
      }

     /*
     ind1->energy = eval_energy(ind1);
     ind1->bandwidth = eval_bandwidth(ind1);

     ind2->energy = eval_energy(ind2);
     ind2->bandwidth = eval_bandwidth(ind2);
		*/

     free(bit_string_temp1);
     free(bit_string_temp2);

     return(0);
}



int uniform_crossover(individual *ind1, individual *ind2)
/* Performs a "uniform crossover".
   Takes pointers to two individuals as arguments and replaces them by the
   children (i.e. the children are stored in ind1 and ind2). */
{
	 int integrity;
	 int *bit_string_temp1;
	 int *bit_string_temp2;

     int choose, i;
     int *bit_string_ind2;
     bit_string_ind2 = (int *) malloc(sizeof(int) * ind2->length);
     bit_string_temp1 = (int *) malloc(sizeof(int) * ind1->length);
     bit_string_temp2 = (int *) malloc(sizeof(int) * ind2->length);

     if (bit_string_ind2 == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (1);  
     }
     /*
     for(i = 0; i < ind2->length; i++)
     {
          bit_string_ind2[i] = ind2->bit_string[i];
     }
	 */
     
     for(i = 0; i < ind2->length; i++)
     {
          bit_string_ind2[i] = ind2->bit_string[i];
          bit_string_temp1[i] = ind1->bit_string[i];
          bit_string_temp2[i] = ind2->bit_string[i];
     }

     for(i = 0; i < ind2->length; i++)
     {
          choose = irand(2); // Probability of 0.5

          if(choose == 1) /* switch around bits */
          { 
        	  bit_string_temp2[i] = ind1->bit_string[i];
        	  bit_string_temp1[i] = bit_string_ind2[i];

              /* ind2->bit_string[i] = ind1->bit_string[i];
               ind1->bit_string[i] = bit_string_ind2[i]; */
          } /* else leave bit as it is */   
     }  

     free(bit_string_ind2);

     /*
     check_integrity(ind1->bit_string);
     check_integrity(ind2->bit_string);

     ind1->energy = eval_energy(ind1);
     ind1->bandwidth = eval_bandwidth(ind1);

     ind2->energy = eval_energy(ind2);
     ind2->bandwidth = eval_bandwidth(ind2);
	   */


     integrity = check_integrity(bit_string_temp1);

     if (integrity == 0)
	  {
		 for(i = 0; i < ind1->length; i++)
		 {
			 ind1->bit_string[i] = bit_string_temp1[i];
		 }

		 ind1->energy = eval_energy(ind1);
		 ind1->bandwidth = eval_bandwidth(ind1);
      }

     integrity = check_integrity(bit_string_temp2);

     if (integrity == 0)
	  {
		 for(i = 0; i < ind2->length; i++)
		 {
			 ind2->bit_string[i] = bit_string_temp2[i];
		 }

		 ind2->energy = eval_energy(ind2);
		 ind2->bandwidth = eval_bandwidth(ind2);
      }

     free(bit_string_temp1);
     free(bit_string_temp2);

     return (0);
}


int irand(int range)
/* Generate a random integer. */
{
     int j;
     j=(int) ((double) range * (double) rand() / (RAND_MAX + 1.0));
     return (j);
}


double drand(double range)
/* Generate a random double. */
{
     double j;
     j=(range * (double) rand() / (RAND_MAX + 1.0));
     return (j);
}


double eval_energy(individual *ind)
/*  */
{
	double aux,res, avb=0;
	int i;
	int* array_solution;
	double * array_if;
	double bdw_total;

	double *TB;
	double *TI;

    double energy = 0;
    double energy_gral = 0;

    double ratio_bdw = 0.00;
    double time_min = 1000000.00;

	aux = 0;
	res = 0;

	array_solution = (int *) malloc(sizeof(int) * nb_apps);
	array_if = (double*) malloc(sizeof(double)* nb_ifaces);

	ind->TB = (double *)malloc(sizeof(double)*nb_ifaces); //SAVE TI TB FOR nb_ifaces INTERFACES
	ind->TI = (double *)malloc(sizeof(double)*nb_ifaces); //SAVE TI TB FOR nb_ifaces INTERFACES

	ind->energy_array = (double *)malloc(sizeof(double)*nb_ifaces);

	TB = (double*) malloc(sizeof(double) * nb_ifaces);
    TI = (double*) malloc(sizeof(double) * nb_ifaces);

	solution_bin_to_dec(ind->bit_string, array_solution);

	for (i=0; i<nb_ifaces; i++)
	{
		array_if[i]=0;
	}

	// array_if has the bandwidth demand for each interface
	for (i=0;i<nb_apps;i++)
	{
		bdw_total = (double)array_apps[i].bdw;
		array_if[array_solution[i]] += bdw_total;

		if ((double)array_apps[i].time < time_min)
		{
			time_min = (double)array_apps[i].time;
		}
	}


	//we calculate the Tb and Ti for each interface
	for (i=0; i<nb_ifaces; i++)
	{

		ratio_bdw = (double) array_if[i] / (double) array_ifaces[i].av_bdw;

		if (ratio_bdw <= 1)
		{
			TB[i] = ratio_bdw * time_min;
			TI[i] = time_min - TB[i];
		} else
		{
			TB[i] = time_min;
			TI[i] = 0;
		}

	}

	//for each interface, we calculate the energy consumed
	 energy = 0;

	  for (i=0; i<nb_ifaces;i++)
	  {
		  if (TB[i]>0) //if the interface is used
		  {
			  if (array_ifaces[i].type == 0) //3G
			  {
				if (TI[i] < array_ifaces[i].timer1)
				{
					energy = (double)(TB[i]+TI[i])*(double)array_ifaces[i].pow1;

				} else if (TI[i] < (array_ifaces[i].timer1+array_ifaces[i].timer2))
				{
					energy = (TB[i]+(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow1 + (TI[i]-(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow2;
				} else
				{
					energy = (TB[i]+(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow1 + ((double)array_ifaces[i].timer2) * (double)array_ifaces[i].pow2 + (TI[i]-(double)array_ifaces[i].timer1-(double)array_ifaces[i].timer2) * (double)array_ifaces[i].pow3;
				}
			  }else //WLAN
			  {
				  if (TI[i] < array_ifaces[i].timer1)
				  {
					 energy =  TB[i]*(double)array_ifaces[i].pow1 + TI[i] *(double) array_ifaces[i].pow2;
				  } else
					 energy = TB[i]*(double)array_ifaces[i].pow1 + (double)array_ifaces[i].timer1 * (double)array_ifaces[i].pow2 + (TI[i]-(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow3;
			  }

			  ind->energy_array[i]=energy;

			  if (energy != 0)
				  energy_gral += ((double)energy/((double)1000)/(TB[i]+TI[i]));
		  }

	  }

	    for (i=0;i<nb_ifaces;i++)
	    {
	    	ind->TB[i]=TB[i];
	    	ind->TI[i]=TI[i];
	    }

	    free(TB);
	    free(TI);
	    free(array_solution);
	    free(array_if);
	    free(ind->energy_array);
	    //free(alpha);

	  return(energy_gral);  //Average Power Consumption


/*  double aux = 0;
  int i, j;

  int * solution;

  double ** db;
  double ** ti;

  double * tmax;

  double *TB;
  double *TI;

  double *alpha;

  double max=0;

  double energy = 0;
  double energy_gral = 0;


	 ind->TB = (double *)malloc(sizeof(double)*nb_ifaces); //SAVE TI TB FOR nb_ifaces INTERFACES
	 ind->TI = (double *)malloc(sizeof(double)*nb_ifaces); //SAVE TI TB FOR nb_ifaces INTERFACES

	 ind->energy_array = (double *)malloc(sizeof(double)*nb_ifaces);

  db = (double **) malloc(sizeof(double *) * nb_ifaces);
    for (i = 0; i < nb_ifaces; i++) {
 	   db[i] = (double *) malloc(sizeof(double) * nb_apps);
    }

  ti = (double **) malloc(sizeof(double *) * nb_ifaces);
	for (i = 0; i < nb_ifaces; i++) {
	   ti[i] = (double *) malloc(sizeof(double) * nb_apps);
	}

  tmax = (double*) malloc(sizeof(double) * nb_ifaces);

  TB = (double*) malloc(sizeof(double) * nb_ifaces);

  TI = (double*) malloc(sizeof(double) * nb_ifaces);

  alpha = (double*) malloc(sizeof(double) * nb_apps);

  solution = (int *)malloc(sizeof(int) * nb_apps);

  solution_bin_to_dec(ind->bit_string, solution);


  for (i=0; i<nb_ifaces;i++)
  {
    for (j=0; j<nb_apps;j++)
    {
	   db[i][j] = 0;
	   ti[i][j] = 0;
    }
  }

  //fill the matrixs db et ti
  for (i=0; i<nb_apps;i++)
  {
	  db[solution[i]][i] = array_apps[i].db;
	  ti[solution[i]][i] = array_apps[i].ti;
  }

  for (i=0; i<nb_ifaces;i++)
  {
	  for (j=0; j<nb_apps;j++)
	  {
		if ((db[i][j]+ti[i][j])>max)
		{
			max = db[i][j]+ti[i][j];
		}
	  }

	  tmax[i]=max;
	  max = 0;
  }

  for (i=0; i<nb_ifaces;i++)
  {
	  for (j=0; j<nb_apps;j++)
	  {
		  if ((db[i][j]+ti[i][j])!=0)
		  {
			  alpha[j]=tmax[i]/(db[i][j]+ti[i][j]);
		  }
	  }
  }

  for (i=0; i<nb_ifaces;i++)
  {
	  for (j=0; j<nb_apps;j++)
	  {
		  if ((db[i][j]+ti[i][j])!=0)
		  {
			  aux += alpha[j]*db[i][j];
		  }

		  if (aux>tmax[i])
			  aux = tmax[i];
	  }

	  TB[i] = aux;
	  TI[i] = tmax[i] - TB[i];
	  aux = 0;
  }


  //calculate energy

  energy = 0;

  for (i=0; i<nb_ifaces;i++)
  {
	  if (array_ifaces[i].type == 0) //3G
	  {
		if (TI[i] < array_ifaces[i].timer1)
		{
			energy = (double)(TB[i]+TI[i])*(double)array_ifaces[i].pow1;

		} else if (TI[i] < (array_ifaces[i].timer1+array_ifaces[i].timer2))
		{
			energy = (TB[i]+(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow1 + (TI[i]-(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow2;
		} else
		{
			energy = (TB[i]+(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow1 + ((double)array_ifaces[i].timer2) * (double)array_ifaces[i].pow2 + (TI[i]-(double)array_ifaces[i].timer1-(double)array_ifaces[i].timer2) * (double)array_ifaces[i].pow3;
		}
	  }else //WLAN
	  {
		  if (TI[i] < array_ifaces[i].timer1)
		  {
			 energy =  TB[i]*(double)array_ifaces[i].pow1 + TI[i] *(double) array_ifaces[i].pow2;
		  } else
			 energy = TB[i]*(double)array_ifaces[i].pow1 + (double)array_ifaces[i].timer1 * (double)array_ifaces[i].pow2 + (TI[i]-(double)array_ifaces[i].timer1) * (double)array_ifaces[i].pow3;
	  }

	  ind->energy_array[i]=energy;

	  if (energy != 0)
		  energy_gral += ((double)energy/((double)1000*tmax[i]));
  }

    for (i=0;i<nb_ifaces;i++)
    {
    	ind->TB[i]=TB[i];
    	ind->TI[i]=TI[i];
    }

  	free(solution);

    for (i=0;i<nb_ifaces;i++)
    {
    	free(db[i]);
    	free(ti[i]);
    }

    free(db);
    free(ti);
    free(tmax);
    free(TB);
    free(TI);
    free(alpha);

  return(energy_gral);  //Average Power Consumption

  */
	//return(rand());
}



double eval_bandwidth(individual *ind)
{
	double aux,res, avb=0;
	int i;
	int* array_solution;
	double * array_if;
	double bdw_total;

	aux = 0;
	res = 0;

	array_solution = (int *) malloc(sizeof(int) * nb_apps);
	array_if = (double*) malloc(sizeof(double)* nb_ifaces);

	solution_bin_to_dec(ind->bit_string, array_solution);

	for (i=0; i<nb_ifaces; i++)
	{
		array_if[i]=0;
	}

	for (i=0;i<nb_apps;i++)
	{
		bdw_total = (double)array_apps[i].bdw;

			//drate = (double)(((double)array_apps[i].sb*1000)/((double)array_apps[i].ti+(double)array_apps[i].db));
		array_if[array_solution[i]] += bdw_total;
	}

	//if ((array_solution[0]==3)&&(array_solution[1]==3)&&(array_solution[2]==3)&&(array_solution[3]==3)&&(array_solution[4]==3)&&(array_solution[5]==3))

	/* SUM OF MOD(OFFER - DEMAND)
	 * for (i=0; i<nb_ifaces; i++)
	{
		if (array_if[i] > 0) // Only if the interface is used
		{
			avb = (double)array_ifaces[i].av_bdw/1000;
			aux+= fabs( avb - array_if[i]);
		}
	}*/

	//SUM OF DEMAND EXCESS
	for (i=0; i<nb_ifaces; i++)
	{
		if (array_if[i] > 0) // Only if the interface is used
		{
			avb = (double)array_ifaces[i].av_bdw;
			aux = (double)array_if[i];

			if (avb-aux<0)
				res +=(avb-aux);
		}
	}

	free(array_solution);
	free(array_if);
	return(fabs(res));
	//return(res);
	//return(rand());
}


individual *new_individual()
/* Creates a random new individual and allocates memory for it.
   Returns a pointer to the new individual.
   Returns NULL if allocation failed.*/
{
     individual *return_ind;
     int i,j;
     int iface_rand;
     int *binary_iface;

     return_ind = (individual *) malloc(sizeof(individual));

     return_ind->bit_string = (int *) malloc(sizeof(int) * length);
	 binary_iface = (int *) malloc(sizeof(int) * len_iface);


     if (return_ind == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (NULL);  
     }
     
     if (return_ind->bit_string == NULL)
	 {
		  log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
		  return (NULL);
	 }


     for (i = 0; i < nb_apps; i++)
     {
    	 iface_rand = irand(nb_ifaces);


    	 dec_bin(iface_rand, binary_iface);

    	 for (j = 0; j < len_iface; j++)
    	 {
    		 return_ind->bit_string[(len_iface*i)+j]=binary_iface[j];
    	 }

     }

	/* for (j=0; j<length; j++)
	 {
		 printf("%d", return_ind->bit_string[j]);
	 }

	 printf("\n");*/
	 free(binary_iface);

     return_ind->length = length;


     return_ind->energy = eval_energy(return_ind);
     return_ind->bandwidth = eval_bandwidth(return_ind);

     return (return_ind);
}


individual *copy_individual(individual *ind)
/* Allocates memory for a new individual and copies 'ind' to this place.
   Returns the pointer to new individual.
   Returns NULL if allocation failed. */
{
     individual *return_ind;
     int i;

     return_ind = (individual *) malloc(sizeof(individual));

     return_ind->bit_string = (int *) malloc(sizeof(int) * length);

     if (return_ind == NULL)
     {
          log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
          return (NULL);  
     }
     

     if (return_ind->bit_string == NULL)
     {
     	  log_to_file(log_file, __FILE__, __LINE__, "variator out of memory");
     	  return (NULL);
     }
     


     for (i = 0; i < length; i++)
    		 return_ind->bit_string[i] = ind->bit_string[i];


     return_ind->energy = eval_energy(return_ind);
     return_ind->bandwidth = eval_bandwidth(return_ind);
     return_ind->length = ind->length;

     return (return_ind);
}



void write_output_file()
/* Writes the index, objective values and bit string of
   all individuals in global_population to 'out_filename'. */
{
     int j,i, current_id, num;
     FILE *fp_out, *fp_decision, *fp_objective;
     individual *temp;
     int * array_sol;
     

     array_sol = (int*)malloc(sizeof(int)*nb_apps);

     fp_out = fopen(outfile, "w");

     fp_decision = fopen("output_dec", "w");
     fp_objective = fopen("output_obj", "w");

     assert(fp_out != NULL);
     assert(fp_decision != NULL);
     assert(fp_objective != NULL);

     current_id = get_first();

    // fprintf(fp_out, "	OFFERED BDW: %.2f	DEMANDED BDW: %.2f\n", total_bdw, total_dem); /* write index */

     while (current_id != -1)
     {       
    	  temp = get_individual(current_id);
          fprintf(fp_out, "%d	", current_id); /* write index */
          fprintf(fp_out, "ENERGY	%.4f	", get_objective_value(current_id, 0));
	      fprintf(fp_out, "BANDWIDTH	%.4f	", get_objective_value(current_id, 1));
	      fprintf(fp_objective, "%.4f	%.4f\n",get_objective_value(current_id, 0), get_objective_value(current_id, 1));

          
	  for (j = 0; j < temp->length; j++)
      {
          fprintf(fp_out, "%d", temp->bit_string[j]);
      }


	  solution_bin_to_dec(temp->bit_string, array_sol);

	  fprintf(fp_out, "	");

	  for (i=0; i<nb_apps;i++)
	   {
		  fprintf(fp_out, "%d", array_sol[i]);
		  fprintf(fp_decision, "%d ",array_sol[i]);
	   }

/*	  fprintf(fp_out, "	TB ");
	  for (i=0; i<nb_ifaces;i++)
	   {
		  fprintf(fp_out, "%.0f-", temp->TB[i]);
	   }

	  fprintf(fp_out, "	TI ");
	  for (i=0; i<nb_ifaces;i++)
	   {
		  fprintf(fp_out, "%.0f-", temp->TI[i]);
	   }

	  fprintf(fp_out, "	EN_ARRAY ");
	  for (i=0; i<nb_ifaces;i++)
	   {
		  fprintf(fp_out, "%.0f-", temp->energy_array[i]);
	   }
 */
	  fprintf(fp_decision, "\n");
      fprintf(fp_out, "\n");

      current_id = get_next(current_id);

     }

     fclose(fp_out);
     fclose(fp_decision);
}


void compute_all_solutions()
{

	int i = 0;
	individual *ind;
	ind = (individual *) malloc(sizeof(individual));
	FILE *fp, *fp_max;
	double energy, bandwidth;

	double max_energy = 0.000;
	double max_band = 0.000;
	double min_energy = 999999999.000;
	double min_band = 999999999.000;

	fp = fopen("all_solutions", "w");
	fp_max = fopen("max", "w");
	if (fp_max == NULL) {
	         printf("I couldn't open max for writing.\n");
	         exit(0);
	      }

	ind->bit_string = (int *) malloc(sizeof(int) * length);
	ind->length = length;

	printf("Start all solutions\n");

	for (i=0; i<pow(2,length); i++)
	{
		dec_bin_long(i,ind->bit_string);

		if (check_integrity(ind->bit_string)==0)
		{
			energy = eval_energy(ind);
			bandwidth = eval_bandwidth(ind);

			if (energy > max_energy)
			{
				max_energy = energy;
			}

			if (bandwidth > max_band)
			{
				max_band = bandwidth;
			}

			if (energy < min_energy)
			{
				min_energy = energy;
			}

			if (bandwidth < min_band)
			{
				min_band = bandwidth;
			}

			int j = 0;

			for (j=0; j<length; j++)
			{
				fprintf(fp, "%d", ind->bit_string[j]);
			}

			fprintf(fp, "	ENERGY	%.3f	BAND	%.3f\n", energy, bandwidth);

		}
	}

	fclose(fp);

	max_e = max_energy;
	max_b = max_band;
	min_e = min_energy;
	min_b = min_band;

	fprintf(fp_max, "%.3lf\n%.3lf\n", max_energy, max_band);

	fclose(fp_max);

	printf("End all solutions\n");

	free(ind->bit_string);
	free(ind);

}

void madm()
{

	int i = 0;
	int l = 0;
	int weights = 40;

	individual *ind;
	ind = (individual *) malloc(sizeof(individual));
	FILE *fp, *fp_mew;

	int * array_sol;
	array_sol = (int*)malloc(sizeof(int)*nb_apps);

	int * ind_array_saw[weights+1];
	int * ind_array_mew[weights+1];

	double energy, bandwidth;

	double max_madm[weights+1];
	double max_madm_mew[weights+1];

	double min_energy[weights+1];
	double min_bandwidth[weights+1];

	double min_energy_mew[weights+1];
	double min_bandwidth_mew[weights+1];

	double rating_energy = 0;
	double rating_bandwidth = 0;

	double madm[weights+1];
	double madm_mew[weights+1];

	for (i=0; i<=weights; i++)
	{
		madm[i] = 0;
		max_madm[i] = 0;
		max_madm_mew[i] = 0;
	}

	ind->bit_string = (int *) malloc(sizeof(int) * length);
	ind->length = length;



	for (i=0; i<=weights; i++)
	{
		ind_array_mew[i] = (int *) malloc(sizeof(int) * length);
		ind_array_saw[i] = (int *) malloc(sizeof(int) * length);

		for (l=0; l<length; l++)
		{
			ind_array_saw[i][l] = 0;
			ind_array_mew[i][l] = 0;
		}
	}

	fp = fopen("madm", "w");
	fp_mew = fopen("madm_mew", "w");

	printf("Start madm\n");

	for (i=0; i<pow(2,length); i++)
	{
		dec_bin_long(i,ind->bit_string);

		if (check_integrity(ind->bit_string)==0)
		{
			energy = eval_energy(ind);
			bandwidth = eval_bandwidth(ind);

			/*
			//Normalization: Linear Scale Transformation => Martinez-Morales / Stevens Navarro -> COST PARAMETERS
			rating_energy = min_e / energy;

			if (bandwidth == 0)
				rating_bandwidth = 1;
			else
				rating_bandwidth = min_b / bandwidth;
			*/


			//Normalization: Linear Scale Transformation MAX-MIN Method => CHAKRABORTY [07] -> COST PARAMETERS
			rating_energy = (max_e - energy) / (max_e - min_e);

			if (bandwidth == 0)
				rating_bandwidth = 1;
			else
				rating_bandwidth = (max_b - bandwidth) / (max_b - min_b);

			//fprintf(fp, "	ENERGY	%.3f	BAND	%.3f", energy, bandwidth);

			int k = 0;

			//Then we calculate SAW and MEW by maximizing the utility functions for different weigths
			for (k=0; k<=weights; k++)
			{
				int j=0;
				double alpha = 1.0 - (double)k/weights;
				double beta = 1.0 - alpha;
				//fprintf(fp, "	ALPHA	%.3f	BETA	%.3f	EN	%.3f	BA	%.3f	MEN	%.3f	MBA	%.3f	RAT_EN	%.3f	RAT_BA	%.3f\n", alpha, beta, energy, bandwidth, max_e, max_b, rating_energy, rating_bandwidth);

				//fprintf(fp_mew, "ALPHA	%.3f	BETA	%.3f	EN	%.3f	BA	%.3f	MEN	%.3f	MBA	%.3f	RAT_EN	%.3f	RAT_BA	%.3f\n", alpha, beta, energy, bandwidth, max_e, max_b, rating_energy, rating_bandwidth);


				madm[k] = (double)alpha*(double)rating_energy + (double)beta*(double)rating_bandwidth;

				if (madm[k] > max_madm[k])
				{
					max_madm[k] = madm[k];
					min_energy[k] = energy;
					min_bandwidth[k] = bandwidth;

					for (j=0; j<length; j++)
					{
						ind_array_saw[k][j] = ind->bit_string[j];

					}
				}

				madm_mew[k] = (pow(rating_energy, (double)alpha)) * (pow(rating_bandwidth,(double)beta));

				if (madm_mew[k] > max_madm_mew[k])
				{
					//fprintf(fp_mew, "	MADM-MEW	%.3f\n", madm_mew[k]);
					max_madm_mew[k] = madm_mew[k];
					min_energy_mew[k] = energy;
					min_bandwidth_mew[k] = bandwidth;

					for (j=0; j<length; j++)
					{
						ind_array_mew[k][j] = ind->bit_string[j];
					}

				}
			}

		}
	}

	for (i=0; i<=weights; i++)
	{
		int j = 0;

		for (j=0; j<length; j++)
		{
			fprintf(fp, "%d", ind_array_saw[i][j]);
		}

		fprintf(fp, "	ENERGY	%.3f	BAND	%.3f	%.3f	ALPHA	%.3f	BETA	%.3f	", min_energy[i],min_bandwidth[i],max_madm[i],1.0-(double)i/weights,(double)i/weights);

		solution_bin_to_dec(ind_array_saw[i], array_sol);

		  for (j=0; j<nb_apps;j++)
		   {
			  fprintf(fp, "%d", array_sol[j]);
		   }

		  fprintf(fp, "\n");
	}



	for (i=0; i<=weights; i++)
	{
		int j = 0;

		for (j=0; j<length; j++)
		{
			fprintf(fp_mew, "%d", ind_array_mew[i][j]);
		}

		fprintf(fp_mew, "	ENERGY	%.3f	BAND	%.3f	%.3f	ALPHA	%.3f	BETA	%.3f	", min_energy_mew[i],min_bandwidth_mew[i],max_madm_mew[i],1.0-(double)i/weights,(double)i/weights);

		solution_bin_to_dec(ind_array_mew[i], array_sol);

		  for (j=0; j<nb_apps;j++)
		   {
			  fprintf(fp_mew, "%d", array_sol[j]);
		   }

		  fprintf(fp_mew, "	A\n");

	}

	printf("End madm\n");

	close(fp);
	close(fp_mew);
	free(ind->bit_string);
	free(ind);

	for (i=0; i<=weights; i++)
	{
		free(ind_array_mew[i]);
		free(ind_array_saw[i]);
	}

}
