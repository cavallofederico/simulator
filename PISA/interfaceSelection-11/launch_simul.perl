#!/usr/bin/perl

$runs = $ARGV[0];

for ($i = 0 ; $i < $runs ; $i++)
{
	$result = `mkdir SIMULATIONS/sim_$i`;
	$result = `cp ifacesel11_param.txt SIMULATIONS/	`;

	$result = `mkdir OUTPUTS`;

	print "LAUNCH SIM $i\n";

	$result = `perl simulator.pl > out_sim_trace`;

	$result = `gnuplot plot_sim`;
	$result = `mv simul_tradeoff.pdf SIMULATIONS/sim_$i/`;
	$result = `mv OUTPUTS SIMULATIONS/sim_$i/`;

	$result = `mv out_sim_trace SIMULATIONS/sim_$i/`;
	$result = `mv output_obj SIMULATIONS/sim_$i/`;
	$result = `mv output_multiobj SIMULATIONS/sim_$i/`;
	$result = `mv output_events SIMULATIONS/sim_$i/`;
	$result = `mv output_energy SIMULATIONS/sim_$i/`;
	$result = `mv output_dec SIMULATIONS/sim_$i/`;

}


